<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailPengajuanSparepartForeignkey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_pengajuan_sparepart', function (Blueprint $table) {
            $table->unsignedBigInteger('pengajuan_id');
            $table->unsignedBigInteger('sparepart_id');

            $table->foreign('pengajuan_id')
                    ->references('id')->on('pengajuan_sparepart')
                    ->onDelete('cascade');
            $table->foreign('sparepart_id')
                    ->references('id')->on('sparepart')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_pengajuan_sparepart', function (Blueprint $table) {
            $table->dropForeign('detail_pengajuan_sparepart_pengajuan_id_foreign');
            $table->dropForeign('detail_pengajuan_sparepart_sparepart_id_foreign');
            $table->dropColumn('pengajuan_id');
            $table->dropColumn('sparepart_id');
        });
    }
}
