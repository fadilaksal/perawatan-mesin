<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPenggantianSparepartForeignkey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penggantian_sparepart', function (Blueprint $table) { 
            $table->unsignedBigInteger("laporan_id");
            $table->unsignedBigInteger("sparepart_id");

            $table->foreign('laporan_id')
                    ->references('id')->on('laporan_kerusakan')
                    ->onDelete('cascade');
            $table->foreign('sparepart_id')
                    ->references('id')->on('sparepart')
                    ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penggantian_sparepart', function (Blueprint $table) {  
            $table->dropForeign('penggantian_sparepart_laporan_id_foreign');
            $table->dropForeign('penggantian_sparepart_sparepart_id_foreign');
            $table->dropColumn('laporan_id');
            $table->dropColumn('sparepart_id'); 
        });
    }
}
