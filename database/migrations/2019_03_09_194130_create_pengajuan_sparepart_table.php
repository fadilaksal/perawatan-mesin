<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanSparepartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_sparepart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("PO");
            $table->string("status");
            $table->integer("leader");
            $table->integer("supervisor");
            $table->integer("manajer");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_sparepart');
    }
}
