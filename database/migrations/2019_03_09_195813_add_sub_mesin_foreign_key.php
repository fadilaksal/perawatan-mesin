<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubMesinForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_mesin', function (Blueprint $table) {
            $table->unsignedBigInteger('mesin_id');

            $table->foreign('mesin_id')
                    ->references('id')
                    ->on('mesin')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_mesin', function (Blueprint $table) {
            $table->dropForeign('sub_mesin_mesin_id_foreign');
            $table->dropColumn('mesin_id');
            
        });
    }
}
