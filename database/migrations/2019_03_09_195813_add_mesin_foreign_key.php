<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMesinForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mesin', function (Blueprint $table) {
            $table->unsignedBigInteger('line_id');
            $table->unsignedBigInteger('group_id');

            $table->foreign('line_id')
                    ->references('id')
                    ->on('lines')
                    ->onDelete('cascade');
            $table->foreign('group_id')
                    ->references('id')->on('group_mesin')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mesin', function (Blueprint $table) {
            $table->dropForeign('mesin_line_id_foreign');
            $table->dropForeign('mesin_group_id_foreign');
            $table->dropColumn('line_id');
            $table->dropColumn('group_id');
        });
    }
}
