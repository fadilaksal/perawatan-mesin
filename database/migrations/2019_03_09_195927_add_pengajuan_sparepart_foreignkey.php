<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPengajuanSparepartForeignkey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengajuan_sparepart', function (Blueprint $table) {
            $table->unsignedBigInteger("laporan_id");

            $table->foreign('laporan_id')
                ->references('id')->on('laporan_kerusakan')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengajuan_sparepart', function (Blueprint $table) {
            $table->dropForeign('pengajuan_sparepart_laporan_id_foreign');
            $table->dropColumn('laporan_id');
        });
    }
}
