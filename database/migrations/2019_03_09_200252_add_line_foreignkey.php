<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLineForeignkey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lines', function (Blueprint $table) { 
            $table->unsignedBigInteger("departemen_id");

            $table->foreign('departemen_id')
                    ->references('id')->on('departemens')
                    ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lines', function (Blueprint $table) { 
            $table->dropForeign('lines_departemen_id_foreign');
            $table->dropColumn('departemen_id');

        });
    }
}
