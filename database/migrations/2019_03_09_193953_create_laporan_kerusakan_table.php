<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporanKerusakanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_kerusakan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('aksi');
            $table->string("keterangan");
            $table->enum('tipe', ['high', 'low']);
            $table->enum('status', ['baru', 'proses', 'selesai']);
            $table->boolean("butuhSparepart")->default(0);
            $table->boolean("butuhJasa")->default(0);
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_kerusakan');
    }
}
