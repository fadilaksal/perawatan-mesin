<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanJasaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_jasa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("vendor");
            $table->string("PO");
            $table->string("status");
            $table->integer("leader");
            $table->integer("supervisor");
            $table->integer("manajer");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_jasa');
    }
}
