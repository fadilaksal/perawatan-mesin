<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSparepartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sparepart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('material');
            $table->string('description');
            $table->integer('quantity');
            $table->enum('base_unit', [
                'BOT', 'BOX', 'CAN', 'KG', 'L', 'M', 'M3', 'mbt', 'PAI', 'PAK', 'PCS', 'ROL', 'SET', 'SHT', 'TUB', 'UN'
            ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sparepart');
    }
}
