<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLaporanKerusakanForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laporan_kerusakan', function (Blueprint $table) {
            $table->unsignedBigInteger("user_id");
            $table->unsignedBigInteger("user_penyelesai_id")->nullable();
            $table->unsignedBigInteger("sub_mesin_id");

            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->foreign('user_penyelesai_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->foreign('sub_mesin_id')
                    ->references('id')->on('sub_mesin')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laporan_kerusakan', function (Blueprint $table) {
            $table->dropForeign('laporan_kerusakan_user_id_foreign');
            $table->dropForeign('laporan_kerusakan_sub_mesin_id_foreign');
            $table->dropForeign('laporan_kerusakan_user_penyelesai_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('user_penyelesai_id');
            $table->dropColumn('sub_mesin_id');
        });
    }
}
