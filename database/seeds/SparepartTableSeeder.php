<?php

use Illuminate\Database\Seeder;
use App\Sparepart;

class SparepartTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sparepart::create([
            'material' => '6000001157',
            'description' => 'Bearing 6202 2Rs',
            'quantity' => '4',
            'base_unit' => 'PCS'
        ]);
    }
}
