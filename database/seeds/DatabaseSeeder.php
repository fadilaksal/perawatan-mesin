<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DepartemenTableSeeder::class);
        $this->call(LineTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SparepartTableSeeder::class);
        $this->call(GroupMesinTableSeeder::class);
        $this->call(MesinTableSeeder::class);
        $this->call(SubMesinTableSeeder::class);
    }
}
