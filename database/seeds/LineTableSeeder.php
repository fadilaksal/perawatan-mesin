<?php

use Illuminate\Database\Seeder;
use App\Departemen;
use App\Line;
class LineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departemen = Departemen::all();
        foreach ($departemen as $key => $value) {
            Line::create([
                'departemen_id' => $value->id,
                'name' => $value->name . " Line 1"
            ]);
        }
    }
}
