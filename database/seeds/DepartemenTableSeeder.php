<?php

use Illuminate\Database\Seeder;
use App\Departemen;

class DepartemenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departemen = array(
            'name' => 'Production Department'
        );
        Departemen::create($departemen);
    }
}
