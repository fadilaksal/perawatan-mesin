<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\Line;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = ['teknisi', 'officer', 'leader', 'supervisor', 'manajer'];
        User::create([
                'line_id' => Line::get()->first()->id,
                'name' => "admin",
                'email' => "admin@gmail.com",
                'password' => Hash::make('password'),
                'employee_id' => rand(0,1000000000) + 1000000000,
                'email_verified_at' => now(),
                'role' => "officer",
                'player_id' => Str::random(40),
                'session' => Str::random(40),
            ]);
        for ($i=0; $i < 5; $i++) { 
            $faker = Faker::create('id_ID');

            User::create([
                'line_id' => Line::get()->first()->id,
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => Hash::make('123123'),
                'employee_id' => rand(0,1000000000) + 1000000000,
                'email_verified_at' => now(),
                'role' => $role[$i],
                'player_id' => Str::random(40),
                'session' => Str::random(40),
            ]);
        }
    }
}
