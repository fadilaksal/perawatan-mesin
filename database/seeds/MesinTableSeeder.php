<?php

use Illuminate\Database\Seeder;
use App\Mesin;
use App\Line;
use App\GroupMesin;

class MesinTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mesin::create([
            'name' => 'BAKING WAFER FRANHAZZ',
            'line_id' => Line::get()->first()->id,
            'group_id' => GroupMesin::where('name', 'baking')->first()->id
        ]);
    }
}
