<?php

use Illuminate\Database\Seeder;
use App\SubMesin;
use App\Mesin;

class SubMesinTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubMesin::create([
            'name' => 'BAKING WAFER FRANHAZZ 1',
            'mesin_id' => Mesin::first()->id,
        ]);
    }
}
