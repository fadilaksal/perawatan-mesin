<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenggantianSparepart extends Model
{
    protected $table = "penggantian_sparepart";
    protected $fillable = ['jumlah', 'laporan_id', 'sparepart_id'];

    public function sparepart()
    {
        return $this->belongsTo('App\Sparepart', 'sparepart_id');
    }
}
