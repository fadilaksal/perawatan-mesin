<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mesin extends Model
{
    protected $table = "mesin";

    protected $fillable = ['name', 'group_id', 'line_id'];
    
    public function group()
    {
        return $this->belongsTo('App\GroupMesin', 'group_id');
    }

    public function line()
    {
        return $this->belongsTo('App\Line');
    }
}
