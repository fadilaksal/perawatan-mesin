<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengajuanJasa extends Model
{
    protected $table = "pengajuan_jasa";
    protected $fillable = ['PO', 'vendor', 'laporan_id', 'leader', 'manajer', 'supervisor', 'status'];

    public function laporan()
    {
        return $this->belongsTo('App\LaporanKerusakan');
    }
}
