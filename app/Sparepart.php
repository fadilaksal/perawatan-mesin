<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sparepart extends Model
{
    protected $table = "sparepart";
    protected $fillable = ['material', 'description', 'quantity', 'base_unit'];
    
}
