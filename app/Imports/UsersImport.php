<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Illuminate\Support\Facades\Hash;

class UsersImport implements ToModel, SkipsOnError
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'employee_id' => $row[0],
            'name' => $row[1],
            'email' => $row[2],
            'email_verified_at' => date('Y-m-d'),
            'password' => Hash::make("123456"),
            'role' => $row[3],
            'line_id' => $row[4]
        ]);
    }

    public function onError(\Throwable $e)
    {
        if ($e->getCode() == 23000) {
            flash("Data duplikat dengan ID Karyawan : " . $e->getBindings()[1])->warning();
        }
    }
}
