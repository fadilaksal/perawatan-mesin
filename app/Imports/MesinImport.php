<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Illuminate\Support\Facades\Hash;
use App\Mesin;

class MesinImport implements ToCollection, SkipsOnError
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $data[] = [
                'name' => $row[0],
                'group_id' => $row[1],
                'line_id' => $row[2]
            ];
        }

        Mesin::insert($data);
    }

    public function onError(\Throwable $e)
    {
        if ($e->getCode() == 23000) {
            flash("Data duplikat dengan ID Karyawan : " . $e->getBindings()[1])->warning();
        }
    }
}
