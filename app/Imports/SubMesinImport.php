<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use App\SubMesin;

class SubMesinImport implements ToCollection
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $data[] = [
                'name' => $row[0],
                'mesin_id' => $row[1]
            ];
        }

        SubMesin::insert($data);
    }

    public function onError(\Throwable $e)
    {
        if ($e->getCode() == 23000) {
            flash("Data duplikat dengan ID Karyawan : " . $e->getBindings()[1])->warning();
        }
    }
}
