<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use App\Sparepart;

class SparepartImport implements ToCollection
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $data[] = [
                'material' => $row[0],
                'description' => $row[1],
                'quantity' => $row[2],
                'base_unit' => $row[3]
            ];
        }

        Sparepart::insert($data);
    }

    public function onError(\Throwable $e)
    {
        if ($e->getCode() == 23000) {
            flash("Data duplikat dengan ID Karyawan : " . $e->getBindings()[1])->warning();
        }
    }
}
