<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMesin extends Model
{
    protected $table = "sub_mesin";

    protected $fillable = ['name', 'mesin_id'];
    
    public function mesin()
    {
        return $this->belongsTo('App\Mesin', 'mesin_id');
    }
}
