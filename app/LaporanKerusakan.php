<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LaporanKerusakan extends Model
{
    protected $table = 'laporan_kerusakan';
    protected $fillable = [
        "aksi",
        "keterangan",
        "tipe",
        "status",
        "butuhSparepart",
        "butuhJasa",
        "image",
        "user_id",
        "sub_mesin_id",
        "user_penyelesai_id",
    ];

    public function sub_mesin()
    {
        return $this->belongsTo('App\SubMesin');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function penggantian_sparepart()
    {
        return $this->hasMany('App\PenggantianSparepart', 'laporan_id');
    }

    public function pengajuan_sparepart()
    {
        return $this->hasOne('App\PengajuanSparepart', 'laporan_id');
    }

    public function jasa()
    {
        return $this->hasOne('App\PengajuanJasa', 'laporan_id');
    }

    public function penyelesai()
    {
        return $this->belongsTo('App\User', 'user_penyelesai_id', 'id');
    }

    public static function reportLaporanDaily($status = null)
    {
        if ($status) {
            $report = self::select('id', 'created_at')
                ->where('created_at', '<', Carbon::tomorrow())
                ->where('created_at', '>=', Carbon::now()->subMonth())
                ->where('status', '=', 'selesai')
                ->get()
                ->groupBy(function($date) {
                    return Carbon::parse($date->created_at)->format('d'); // grouping by day
                });
        } else {
            $report = self::select('id', 'created_at')
                ->where('created_at', '<', Carbon::tomorrow())
                ->where('created_at', '>=', Carbon::now()->subMonth())
                ->get()
                ->groupBy(function($date) {
                    return Carbon::parse($date->created_at)->format('d'); // grouping by day
                });
        }
        return $report;
    }

    public static function reportByUser($userId)
    {
        $report = self::select('id', 'created_at')
                ->where('created_at', '<', Carbon::tomorrow())
                ->where('created_at', '>=', Carbon::now()->subYear())
                ->where('user_id', '=', $userId)
                ->get()
                ->groupBy(function($date) {
                    return Carbon::parse($date->created_at)->format('m'); // grouping by day
                });
        return $report;
    }

    public static function reportFinishByUser($userId)
    {
        $report = self::select('id', 'created_at')
                ->where('created_at', '<', Carbon::tomorrow())
                ->where('created_at', '>=', Carbon::now()->subYear())
                ->where('status', '=', 'selesai')
                ->where('user_penyelesai_id', '=', $userId)
                ->get()
                ->groupBy(function($date) {
                    return Carbon::parse($date->created_at)->format('m'); // grouping by day
                });
        return $report;
    }
}
