<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupMesin extends Model
{
    protected $table = "group_mesin";

    protected $fillable = ['name'];

    public function mesin()
    {
        return $this->hasMany('App\Mesin', 'group_id', 'id');
    }
}
