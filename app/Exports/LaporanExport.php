<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use App\LaporanKerusakan;

class LaporanExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    public function headings(): array
    {
        return [
            'Tanggal Laporan',
            'Aksi',
            'Keterangan',
            'Tipe',
            'Status',
            'Butuh Sparepart',
            'Butuh Jasa',
            'Pelapor',
            'Penyelesai',
            'Sub Mesin',
        ];
    }

    public function map($laporan): array
    {
        return [
            $laporan->created_at,
            $laporan->aksi,
            $laporan->keterangan,
            $laporan->tipe,
            $laporan->status,
            $laporan->butuhSparepart,
            $laporan->butuhJasa,
            $laporan->user->name,
            $laporan->user_penyelesai_id != null ? $laporan->penyelesai->name : "",
            $laporan->sub_mesin->name
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return LaporanKerusakan::all();
    }
}
