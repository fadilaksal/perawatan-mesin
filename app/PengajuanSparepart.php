<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengajuanSparepart extends Model
{
    protected $table = "pengajuan_sparepart";
    protected $fillable = ['PO', 'status', 'leader', 'supervisor', 'manajer', 'laporan_id'];

    public function detail()
    {
        return $this->hasMany('App\DetailPengajuanSparepart', 'pengajuan_id');
    }

    public function laporan()
    {
        return $this->belongsTo('App\LaporanKerusakan');
    }
}
