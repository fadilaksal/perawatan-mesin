<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departemen extends Model
{
    protected $fillable = ['name'];

    public function line()
    {
        return $this->hasMany('App\Line');
    }
}
