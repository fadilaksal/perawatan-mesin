<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Line extends Model
{
    protected $fillable = [ 'departemen_id', 'name' ];

    public function departemen()
    {
        return $this->belongsTo('App\Departemen');
    }

    public function mesin()
    {
        return $this->hasMany('App\Mesin');
    }
}
