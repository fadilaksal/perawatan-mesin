<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPengajuanSparepart extends Model
{
    protected $table = "detail_pengajuan_sparepart";
    protected $fillable = ['jumlah', 'pengajuan_id', 'sparepart_id'];

    public function sparepart()
    {
        return $this->belongsTo('App\Sparepart', 'sparepart_id');
    }

    public function pengajuan()
    {
        return $this->belongsTo('App\PengajuanSparepart', 'pengajuan_id');
    }
}
