<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\LaporanKerusakan;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'session' => $this->session,
            'line_id' => $this->line_id,
            'line' => new LineResource($this->whenLoaded('line')),
            'jml_laporan_kerusakan' => count($this->laporan),
            'jml_laporan_diselesaikan' => LaporanKerusakan::where('user_penyelesai_id', $this->id)->count()
        ];
    }
}
