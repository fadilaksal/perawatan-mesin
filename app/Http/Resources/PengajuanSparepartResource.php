<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PengajuanSparepartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'PO' => $this->PO,
            'status' => $this->status,
            'leader' => $this->leader,
            'supervisor' => $this->supervisor,
            'manajer' => $this->manajer,
            'laporan' => new LaporanKerusakanResource($this->whenLoaded('laporan')),
            'detail' => DetailPengajuanSparepartResource::collection($this->whenLoaded('detail')),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
