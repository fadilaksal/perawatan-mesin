<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PengajuanJasaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'vendor' => $this->vendor,
            'PO' => $this->PO,
            'status' => $this->status,
            'leader' => $this->leader,
            'supervisor' => $this->supervisor,
            'manajer' => $this->manajer,
            'laporan' => new LaporanKerusakanResource($this->whenLoaded('laporan')),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
