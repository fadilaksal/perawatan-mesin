<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PenggantianSparepartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'jumlah' => $this->jumlah,
            'laporan' => new LaporanKerusakanResource($this->whenLoaded('laporan')),
            'sparepart' => new SparepartResource($this->whenLoaded('sparepart'))
        ];
    }
}
