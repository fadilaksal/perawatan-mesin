<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PenyelesaianLaporanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'laporan' => new LaporanKerusakanResource($this->whenLoaded('laporan')),
            'user' => new UserResource($this->whenLoaded('user')),
            'created_at' => $this->created_at->format('d-m-Y H:i:s')
        ];
    }
}
