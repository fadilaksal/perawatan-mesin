<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LaporanKerusakanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'aksi' => $this->aksi,
            'keterangan' => $this->keterangan,
            'tipe' => $this->tipe,
            'status' => $this->status,
            'butuhSparepart' => $this->butuhSparepart,
            'butuhJasa' => $this->butuhJasa,
            'image' => $this->image != "" ? url('storage/img/laporan-kerusakan/' . $this->image) : "",
            'user' => new UserResource($this->whenLoaded('user')),
            'sub_mesin' => new SubMesinResource($this->whenLoaded('sub_mesin')),
            'penyelesai' => new UserResource($this->whenLoaded('penyelesai')),
            'penggantianSparepart' => PenggantianSparepartResource::collection($this->whenLoaded('penggantian_sparepart')),
            'pengajuanJasa' => new PengajuanJasaResource($this->whenLoaded('jasa')),
            'pengajuanSparepart' => new PengajuanSparepartResource($this->whenLoaded('pengajuan_sparepart')),
            'created_at' => $this->created_at->format('d-m-Y H:i:s')
        ];
    }
}
