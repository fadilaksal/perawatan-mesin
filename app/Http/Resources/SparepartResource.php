<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SparepartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'material' => $this->material,
            'description' => $this->description,
            'quantity' => $this->quantity,
            'base_unit' => $this->base_unit,
            'created_at' => $this->created_at,
        ];
    }
}
