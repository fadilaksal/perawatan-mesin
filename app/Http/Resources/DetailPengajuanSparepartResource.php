<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailPengajuanSparepartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'jumlah' => $this->jumlah,
            'pengajuan_id' => $this->pengajuan_id,
            'sparepart_id' => $this->sparepart_id,
            'sparepart' => new SparepartResource($this->whenLoaded('sparepart'))
        ];
    }
}
