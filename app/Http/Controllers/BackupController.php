<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BackupManager\Filesystems\Destination;
use BackupManager\Manager;

class BackupController extends Controller
{
    
    public function index()
    {
        $backups = [];
        if (file_exists(storage_path('app/backup/db'))) {
            $file = \File::allFiles(storage_path('app/backup/db'));
            foreach($file as $data){
                array_push($backups,
                [
                    'name' => $data->getFileName(),
                    'size' => $data->getSize(),
                    'created' => date("F d Y H:i:s.",filemtime($data)),
                ]
                );
            }
            usort($backups, function($a, $b) {
                return -1 * strcmp($a['created'], $b['created']);
            });
        }
        return view('layouts.pages.backup.index', compact('backups'));
    }
    
    public function store(Request $request)
    {
        if (!file_exists(storage_path('app/backup/db/') . $request->name.'.gz' )) {
            $manager = app()->make(Manager::class);
            $fileName = $request->name;
            $manager->makeBackup()->run('mysql', [
                    new Destination('local', 'backup/db/' . $fileName)
                ], 'gzip');
            flash("Berhasil menambah backup database")->success();
        } else {
            flash("Gagal menambah backup database")->error();
        }
        return redirect()->back();
    }

    public function destroy($fileName)
    {
        if (file_exists(storage_path('app/backup/db/') . $fileName)) {
            unlink(storage_path('app/backup/db/') . $fileName);
            flash("Berhasil menghapus backup database")->success();
        } else {
            flash("Gagal menghapus backup database")->error();
        }
        return redirect()->back();
    }

    public function download($fileName)
    {
        return response()->download(storage_path('app/backup/db/') . $fileName);
    }

    public function restore($fileName)
    {
        if (file_exists(storage_path('app/backup/db/') . $fileName )) {
            $manager = app()->make(Manager::class);
            $manager->makeRestore()->run('local', 'backup/db/' . $fileName, 'mysql', 'gzip');
            
            flash("Berhasil backup database")->success();
        } else {
            flash("Gagal backup database")->error();
        }
        return redirect()->back();
    }
}
