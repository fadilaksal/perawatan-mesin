<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PengajuanJasa;

class PengajuanJasaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengajuanJasa = PengajuanJasa::with(['laporan'])->paginate(15);
        return view('layouts.pages.pengajuan_jasa.index', compact('pengajuanJasa'));
    }

    public function show($id)
    {
        $pengajuan = PengajuanJasa::findOrFail($id);
        return view('layouts.pages.pengajuan_jasa.detail', compact('pengajuan'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'vendor' => 'required|string|max:191',
            'po' => 'required|string|max:191',
            'status' => 'required|string|max:191'
        ]);
        
        $pengajuanJasa = PengajuanJasa::findOrFail($id);
        $pengajuanJasa->vendor = $request->vendor;
        $pengajuanJasa->PO = $request->po;
        $pengajuanJasa->status = $request->status;
        $pengajuanJasa->leader = auth()->user()->id;
        $pengajuanJasa->supervisor = auth()->user()->id;
        $pengajuanJasa->manajer = auth()->user()->id;

        $pengajuanJasa->save();

        flash("Berhasil mengubah pengajuan jasa" . $pengajuanJasa->laporan->keterangan)->success();

        return redirect()->route('pengajuan-jasa');
    }
}
