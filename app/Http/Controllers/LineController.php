<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departemen;
use App\Line;

class LineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $line = Line::paginate(15);
        $departemen = Departemen::all();
        return view('layouts.pages.line.index', compact('line', 'departemen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'departemen' => 'required|int|exists:departemens,id',
            'name' => 'required|string|max:191'
        ]);
        
        $line = Line::create([
            'departemen_id' => $request->departemen,
            'name' => $request->name
        ]);

        flash("Berhasil menambahkan data line : $request->name")->success();

        return redirect()->route('line');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'departemen' => 'required|int|exists:departemens,id',
            'name' => 'required|string|max:191'
        ]);

        $line = Line::findOrFail($id);
        
        $line->departemen_id = $request->departemen_id != null ? $request->departemen_id : $line->departemen_id;
        $line->name = $request->name != null ? $request->name : $line->name;
        
        $line->update();

        flash("Berhasil mengubah data line : $request->name")->success();

        return redirect()->route('line');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $line = Line::findOrFail($id);

        flash("Berhasil menghapus line : $line->name")->success();

        $line->delete();

        return redirect()->route('line');
    }
}
