<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LaporanKerusakan;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $laporan = LaporanKerusakan::take(10)->with('user')->orderBy('created_at', 'desc')->get();

        return view('layouts.pages.dashboard', compact('laporan'));
    }
}
