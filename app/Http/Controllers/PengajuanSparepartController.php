<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PengajuanSparepart;

class PengajuanSparepartController extends Controller
{
   public function index()
    {
        $pengajuanSparepart = PengajuanSparepart::with(['laporan'])->paginate(15);
        return view('layouts.pages.pengajuan_sparepart.index', compact('pengajuanSparepart'));
    }

    public function show($id)
    {
        $pengajuan = PengajuanSparepart::findOrFail($id);
        return view('layouts.pages.pengajuan_sparepart.detail', compact('pengajuan'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'PO' => 'required|string|max:191',
            'status' => 'required|string|max:191'
        ]);
        
        $pengajuanSparepart = PengajuanSparepart::findOrFail($id);

        $pengajuanSparepart->update($request->all());

        flash("Berhasil mengubah pengajuan sparepart " . $pengajuanSparepart->laporan->keterangan)->success();

        return redirect()->route('pengajuan-sparepart');
    }
}
