<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LaporanKerusakan;
use App\Exports\LaporanExport;

class LaporanKerusakanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $laporan = LaporanKerusakan::with(['user', 'sub_mesin'])->paginate(15);
        return view('layouts.pages.laporan.index', compact('laporan'));
    }

    public function show($id)
    {
        $laporan = LaporanKerusakan::findOrFail($id);
        return view('layouts.pages.laporan.show', compact('laporan'));
    }

    public function export()
    {
        $export = new LaporanExport();
        return $export->download("laporan kerusakan.xlsx");
    }
}