<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departemen;

class DepartemenController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departemen = Departemen::paginate(15);
        return view('layouts.pages.departemen.index', compact('departemen'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191'
        ]);
        
        $departemen = Departemen::create($request->all());

        flash("Berhasil menambah departemen $departemen->name")->success();

        return redirect()->route('departemen');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191'
        ]);
        
        $departemen = Departemen::findOrFail($id);

        $departemen->update($request->all());

        flash("Berhasil mengubah departemen $departemen->name")->success();

        return redirect()->route('departemen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departemen = Departemen::findOrFail($id);

        flash("Berhasil menghapus departemen $departemen->name")->success();

        $departemen->delete();

        return redirect()->route('departemen');
    }
}
