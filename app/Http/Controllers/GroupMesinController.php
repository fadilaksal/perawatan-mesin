<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GroupMesin;

class GroupMesinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groupMesin = GroupMesin::paginate(15);
        return view('layouts.pages.mesin.group', compact('groupMesin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191'
        ]);
        
        $groupMesin = GroupMesin::create($request->all());

        flash("Berhasil menambah group $groupMesin->name")->success();

        return redirect()->route('group-mesin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191'
        ]);
        
        $groupMesin = GroupMesin::findOrFail($id);

        $groupMesin->update($request->all());

        flash("Berhasil mengubah group $groupMesin->name")->success();

        return redirect()->route('group-mesin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $groupMesin = GroupMesin::findOrFail($id);

        flash("Berhasil menghapus group $groupMesin->name")->success();

        $groupMesin->delete();

        return redirect()->route('group-mesin');
    }
}
