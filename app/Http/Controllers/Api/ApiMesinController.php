<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Mesin;
use App\Http\Resources\MesinResource;

class ApiMesinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil laporan",
            'data' => MesinResource::collection(Mesin::with(['group', 'line', 'line.departemen'])->get())
        ];
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil laporan",
            'data' => new MesinResource(Mesin::with(['group', 'line', 'line.departemen'])->findOrFail($id))
        ];
        return $response;
    }

    public function getByLineId($lineId)
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil Mesin",
            'data' => MesinResource::collection(Mesin::where('line_id', $lineId)->get())
        ];
        return $response;
    }
}
