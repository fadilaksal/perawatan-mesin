<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use DB;

use App\LaporanKerusakan;
use App\PenyelesaianLaporanKerusakan;
use App\Http\Resources\PenyelesaianLaporanResource;

class ApiPenyelesaianLaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $response = [
            'status' => true,
            'message' => "berhasil mengambil laporan",
            'data' => PenyelesaianLaporanResource::collection(PenyelesaianLaporanKerusakan::with(['laporan', 'user', 'laporan.sub_mesin', 'laporan.sub_mesin.mesin', 'laporan.sub_mesin.mesin.line', 'laporan.sub_mesin.mesin.line.departemen'])->get())
        ];
        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
