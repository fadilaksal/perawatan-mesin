<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use OneSignal;
use Carbon\Carbon;
use App\PengajuanSparepart;
use App\User;
use App\Http\Resources\PengajuanSparepartResource;

class ApiPengajuanSparepartController extends Controller
{
    private $responseStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil data pengajuan",
            'data' => PengajuanSparepartResource::collection(PengajuanSparepart::with(['laporan', 'laporan.user', 'laporan.sub_mesin', 'laporan.sub_mesin.mesin', 'laporan.sub_mesin.mesin.line', 'laporan.sub_mesin.mesin.line.departemen'])->get())
        ];
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil data pengajuan",
            'data' => new PengajuanSparepartResource(PengajuanSparepart::with(['laporan', 'laporan.user', 'laporan.sub_mesin.mesin.line', 'detail', 'detail.sparepart'])->findOrFail($id))
        ];
        return $response;
    }
    
    public function confirm(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            "user_id" => 'int|required',
            "tipe" => 'string|required',
        ]);
        
        if ($validator->fails()) {
            $response = [
                'message' => implode(",",$validator->messages()->all()),
                'data' => null,
                'status' => false,
            ];
            $this->responseStatus = 400;
        } else {
            $data = [
                "$request->tipe" => $request->user_id,
                'status' => 'proses'
            ];

            $pengajuanSparepart = PengajuanSparepart::where('id', $id)->update($data);
            if ($pengajuanSparepart) {
                $pengajuanSparepart = PengajuanSparepart::find($id);

                $user = User::find($request->user_id);
                
                $pelaporId = $pengajuanSparepart->laporan->user_id;
                $pelapor = User::find($pelaporId);

                Onesignal::sendNotificationToUser(
                    "Pengajuan sparepart telah dikonfirmasi oleh $user->name selaku $request->tipe",
                    $pelapor->player_id
                );

                $response = [
                    'message' => "Konfirmasi pengajuan berhasil",
                    'data' => new PengajuanSparepartResource(PengajuanSparepart::with(['laporan', 'laporan.user', 'laporan.sub_mesin.mesin.line', 'detail', 'detail.sparepart'])->findOrFail($id)),
                    'status' => true,
                ];
            } else {
                $response = [
                    'message' => "Konfirmasi pengajuan gagal, silahkan coba lagi",
                    'data' => null,
                    'status' => true,
                ];
            }
        }
        return response()->json($response, $this->responseStatus);
    }

    public function getPengajuanByDate($firstDate, $lastDate)
    {
        $pengajuan = PengajuanSparepart::with(['laporan', 'laporan.user', 'laporan.sub_mesin', 'laporan.sub_mesin.mesin', 'laporan.sub_mesin.mesin.line', 'laporan.sub_mesin.mesin.line.departemen'])
                                    ->whereDate('created_at', '>=',  Carbon::parse($firstDate)->format('Y-m-d'))
                                    ->whereDate('created_at', '<=', Carbon::parse($lastDate)->format('Y-m-d'))
                                    ->orderBy('created_at', 'desc')
                                    ->get();
        $pengajuanResource = PengajuanSparepartResource::collection($pengajuan);

        $response = [
            'status' => true,
            'message' => "berhasil mengambil data pengajuan",
            'data' => $pengajuanResource
        ];
        return $response;
    }
}
