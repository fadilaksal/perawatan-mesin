<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

use Validator;
use Hash;
use Auth;

use App\User;
use App\Http\Resources\UserResource;

class ApiAuthorizationController extends Controller
{
    private $responseStatus = 200;

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'string|required',
            'password' => 'string|required'
        ]);
        
        if ($validator->fails()) {
            $response = [
                    'message' => $validator->messages(),
                    'data' => null,
                    'status' => false,
                ];
            $this->responseStatus = 400;
        } else{
            // $user = User::where('email', $request->email)->where('password', Hash::make($request->password))->first();
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = User::with('line', 'line.departemen')->find(Auth::user()->id); 
                $user->session = Str::random(40);
                $user->player_id = $request->playerId;
                $user->save();
                
                $response = [
                    'message' => "Login Sukses",
                    'data' => new UserResource($user),
                    'status' => true,
                ];
            } else {
                $response = [
                    'message' => "Login Gagal, Silahkan cek email dan password",
                    'data' => null,
                    'status' => false,
                ];

                $this->responseStatus = 400;
            }
        }

        return response()->json($response, $this->responseStatus);
    }

    public function logout($id)
    {
        $user = User::find($id);

        if ($user = User::find($id)) {
            $user->session = null;
        
            $user->save();

            $response = [
                'message' => "Logout berhasil",
                'data' => null,
                'status' => true,
            ];
        } else {
            $response = [
                'message' => "Logout Gagal, user tidak ditemukan",
                'data' => null,
                'status' => false,
            ];
            $this->responseStatus = 400;
        }

        return response()->json($response, $this->responseStatus);
    }
}
