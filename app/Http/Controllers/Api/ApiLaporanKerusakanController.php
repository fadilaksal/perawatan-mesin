<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use DB;

use App\LaporanKerusakan;
use App\Sparepart;
use App\DetailPengajuanSparepart;
use App\PenggantianSparepart;
use App\PengajuanSparepart;
use App\PengajuanJasa;
use Carbon\Carbon;
use OneSignal;
use App\Http\Resources\LaporanKerusakanResource;
use Illuminate\Http\UploadedFile;

class ApiLaporanKerusakanController extends Controller
{
    private $responseStatus = 200;
    
    public function index()
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil laporan",
            'data' => LaporanKerusakanResource::collection(LaporanKerusakan::with(['sub_mesin', 'user', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line.departemen'])->get())
        ];
        return $response;
    }

    public function getLastLaporan()
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil laporan",
            'data' => LaporanKerusakanResource::collection(LaporanKerusakan::with(['sub_mesin', 'user', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line.departemen'])->take(5)->orderBy('created_at', 'desc')->get())
        ];
        return $response;
    }

    public function show($id)
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil laporan",
            'data' => new LaporanKerusakanResource(LaporanKerusakan::with(['sub_mesin', 'user', 'penyelesai', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line.departemen', 'penggantian_sparepart.sparepart', 'jasa', 'pengajuan_sparepart', 'pengajuan_sparepart.detail.sparepart'])->findOrFail($id))
        ];
        return $response;
    }

    public function getLaporanSelesai()
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil laporan",
            'data' => LaporanKerusakanResource::collection(LaporanKerusakan::with(['sub_mesin', 'user', 'penyelesai', 'sub_mesin.mesin', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line', 'sub_mesin.mesin.line.departemen'])->where('status', 'selesai')->orderBy('created_at', 'desc')->get())
        ];
        return $response;
    }

    public function getLaporanSelesaiByDate($firstDate, $lastDate)
    {
        $laporan = LaporanKerusakan::with(['sub_mesin', 'user', 'penyelesai', 'sub_mesin.mesin', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line', 'sub_mesin.mesin.line.departemen'])
                                    ->where('status', 'selesai')
                                    ->whereDate('created_at', '>=',  Carbon::parse($firstDate)->format('Y-m-d'))
                                    ->whereDate('created_at', '<=', Carbon::parse($lastDate)->format('Y-m-d'))
                                    ->orderBy('created_at', 'desc')
                                    ->get();
        $laporanResource = LaporanKerusakanResource::collection($laporan);
        $response = [
            'status' => true,
            'message' => "berhasil mengambil laporan",
            'data' => $laporanResource
        ];
        return $response;
    }

    public function prosesLaporan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "laporan_id" => 'int|required',
            "user_penyelesai_id" => 'int|required',
        ]);
        
        if ($validator->fails()) {
            $response = [
                'message' => implode(",",$validator->messages()->all()),
                'data' => null,
                'status' => false,
            ];
            $this->responseStatus = 400;
        } else {
            $laporan = LaporanKerusakan::where('id', $request->laporan_id)->first();
            $laporan->status = "proses";
            $laporan->user_penyelesai_id = $request->user_penyelesai_id;

            $laporan->update();

            $response = [
                'message' => "Laporan berhasil dikerjakan",
                'data' => new LaporanKerusakanResource(LaporanKerusakan::with(['sub_mesin', 'user', 'penyelesai', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line.departemen', 'penggantian_sparepart.sparepart', 'jasa', 'pengajuan_sparepart', 'pengajuan_sparepart.detail.sparepart'])->findOrFail($request->laporan_id)),
                'status' => true
            ];
        }
        return $response;
    }

    public function finishLaporan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "laporan_id" => 'int|required',
            "user_penyelesai_id" => 'int|required',
        ]);
        
        if ($validator->fails()) {
            $response = [
                'message' => implode(",",$validator->messages()->all()),
                'data' => null,
                'status' => false,
            ];
            $this->responseStatus = 400;
        } else {
            $laporan = LaporanKerusakan::where('id', $request->laporan_id)->where('user_penyelesai_id', $request->user_penyelesai_id)->first();
            $laporan->status = "selesai";

            $laporan->update();

            $response = [
                'message' => "Laporan berhasil dikerjakan",
                'data' => new LaporanKerusakanResource(LaporanKerusakan::with(['sub_mesin', 'user', 'penyelesai', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line.departemen', 'penggantian_sparepart.sparepart', 'jasa', 'pengajuan_sparepart', 'pengajuan_sparepart.detail.sparepart'])->findOrFail($request->laporan_id)),
                'status' => true
            ];
        }
        return $response;
    }

    private function addPengajuanJasa($request, $laporan)
    {
        if (!$request->butuhJasa) {
            $response = [
                'message' => "Berhasil menambah laporan dengan pengajuan",
                'data' => new LaporanKerusakanResource(LaporanKerusakan::with(['sub_mesin', 'user', 'sub_mesin.mesin', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line', 'sub_mesin.mesin.line.departemen'])->findOrFail($laporan->id)),
                'status' => true,
            ];

            return true;
        }

        $pengajuanJasa = PengajuanJasa::create([
            'status' => 'baru',
            'vendor' => '',
            'PO' => '',
            'leader' => '0',
            'supervisor' => '0',
            'manajer' => '0',
            'laporan_id' => $laporan->id
        ]);

        if ($pengajuanJasa) {
            return true;
        } else {
            return false;
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "aksi" => 'string|required',
            "keterangan" => 'string|required',
            "tipe" => 'string|required',
            "status" => 'string|required',
            "butuhSparepart" => 'int|required',
            "butuhJasa" => 'int|required',
            "userId" => 'int|required',
            "subMesinId" => 'int|required'
        ]);

        $image = "";
        
        if ($request->hasFile('image')) {
            $image = $this->savePhoto($request->file('image'));
        }

        $data = [
            "aksi" => $request->aksi,
            "keterangan" => $request->keterangan,
            "tipe" => $request->tipe,
            "status" => $request->status,
            "butuhSparepart" => $request->butuhSparepart,
            "butuhJasa" => $request->butuhJasa,
            "image" => $image,
            "user_id" => $request->userId,
            "sub_mesin_id" => $request->subMesinId,
        ];
        
        if ($validator->fails()) {
            $response = [
                'message' => implode(",",$validator->messages()->all()),
                'data' => null,
                'status' => false,
            ];
            $this->responseStatus = 400;
        } else {
            $laporan = LaporanKerusakan::create($data);
            if($laporan) {
                if ($this->addPengajuanJasa($request, $laporan)) {
                    OneSignal::sendNotificationToAll(
                        "Ada Laporan Kerusakan Baru '$laporan->keterangan' pada mesin: ".$laporan->sub_mesin->name." oleh ".$laporan->user->name,
                    );
                    $response = [
                        'message' => "Berhasil menambah laporan",
                        'data' => new LaporanKerusakanResource(LaporanKerusakan::with(['sub_mesin', 'user', 'sub_mesin.mesin', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line', 'sub_mesin.mesin.line.departemen'])->findOrFail($laporan->id)),
                        'status' => true,
                    ];
                } else {
                    $response = [
                        'message' => "Gagal menambah laporan",
                        'data' => null,
                        'status' => false,
                    ];
                }
            } else {
                $response = [
                    'message' => "Gagal menambah laporan",
                    'data' => null,
                    'status' => false,
                ];
            }
        }

        return response()->json($response, $this->responseStatus);
    }

    public function storeWithSparepart(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "aksi" => 'string|required',
            "keterangan" => 'string|required',
            "tipe" => 'string|required',
            "status" => 'string|required',
            "butuhSparepart" => 'int|required',
            "butuhJasa" => 'int|required',
            "userId" => 'int|required',
            "subMesinId" => 'int|required'
        ]);

        $image = "";

        if ($request->hasFile('image')) {
            $image = $this->savePhoto($request->file('image'));
        }

        $data = [
            "aksi" => $request->aksi,
            "keterangan" => $request->keterangan,
            "tipe" => $request->tipe,
            "status" => $request->status,
            "butuhSparepart" => $request->butuhSparepart,
            "butuhJasa" => $request->butuhJasa,
            "image" => $image,
            "user_id" => $request->userId,
            "sub_mesin_id" => $request->subMesinId,
        ];
        
        // jika validasi gagal, return failed
        if ($validator->fails()) {
            $response = [
                'message' => implode(",",$validator->messages()->all()),
                'data' => null,
                'status' => false,
            ];

            return response()->json($response, 400);
        } 

        $laporan = LaporanKerusakan::create($data);

        // jika laporan tidak masuk db, return failed
        if(!$laporan) {
            $response = [
                'message' => "Gagal menambah laporan",
                'data' => null,
                'status' => false,
            ];
            
            return response()->json($response, 400);
        }

        $sparepartId = json_decode($request->sparepartId, TRUE);
        $sparepartQty = json_decode($request->sparepartQty, TRUE);

        for ($i=0; $i < count($sparepartId); $i++) { 
            $penggantianSparepart[$i] = [
                'laporan_id' => $laporan->id,
                'sparepart_id' => $sparepartId[$i],
                'jumlah' => $sparepartQty[$i],
                'created_at'=>date('Y-m-d H:i:s'), 
                'updated_at'=> date('Y-m-d H:i:s')
            ];
        }

        $penggantianSparepart = PenggantianSparepart::insert($penggantianSparepart);
        // jika penggantian sparepart, return failed
        if(!$penggantianSparepart) {
            $response = [
                'message' => "Gagal menambah laporan",
                'data' => null,
                'status' => false,
            ];

            return response()->json($response, 400);
        }
        
        // jika tanpa status pengajuan maka berhasil menambah laporan
        if (!$request->statusPengajuan) {
            if ($this->addPengajuanJasa($request, $laporan)) {
                $response = [
                    'message' => "Berhasil menambah laporan",
                    'data' => new LaporanKerusakanResource(LaporanKerusakan::with(['sub_mesin', 'user', 'sub_mesin.mesin', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line', 'sub_mesin.mesin.line.departemen'])->findOrFail($laporan->id)),
                    'status' => true,
                ];
            } else {
                $response = [
                    'message' => "Gagal menambah laporan",
                    'data' => null,
                    'status' => false,
                ];
            }
            
            return response()->json($response, 200);
        }

        $pengajuanSparepart = PengajuanSparepart::create([
            'PO' => '',
            'leader' => 0,
            'supervisor' => 0,
            'manajer' => 0,
            'status' => 'proses',
            'laporan_id' => $laporan->id
        ]);
        if (!$pengajuanSparepart) {
            $response = [
                'message' => "Gagal menambah laporan",
                'data' => null,
                'status' => false,
            ];
            return response()->json($response, 400);
        }

        $j = 0;
        for ($i=0; $i < count($sparepartId); $i++) { 
            $sparepart = Sparepart::find($sparepartId[$i]);

            if($sparepart->quantity < $sparepartQty[$i]) {
                $detailPengajuanSparepart[$j] = [
                    'pengajuan_id' => $pengajuanSparepart->id,
                    'sparepart_id' => $sparepart->id,
                    'jumlah' => $sparepartQty[$i],
                    'created_at'=>date('Y-m-d H:i:s'), 
                    'updated_at'=> date('Y-m-d H:i:s')
                ];
                $j++;
            }
        }

        $detailPengajuanSparepart = DetailPengajuanSparepart::insert($detailPengajuanSparepart);

        if (!$detailPengajuanSparepart) {
            $response = [
                'message' => "Gagal menambah laporan",
                'data' => null,
                'status' => false,
            ];

            return response()->json($response, 400);
        } 


        if ($this->addPengajuanJasa($request, $laporan)) {
            OneSignal::sendNotificationToAll(
                "Ada Laporan Kerusakan Baru '$laporan->keterangan' pada mesin: ".$laporan->sub_mesin->name." oleh ".$laporan->user->name,
            );
            $response = [
                'message' => "Berhasil menambah laporan",
                'data' => new LaporanKerusakanResource(LaporanKerusakan::with(['sub_mesin', 'user', 'sub_mesin.mesin', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line', 'sub_mesin.mesin.line.departemen'])->findOrFail($laporan->id)),
                'status' => true,
            ];
        } else {
            $response = [
                'message' => "Gagal menambah laporan",
                'data' => null,
                'status' => false,
            ];
        }

        return response()->json($response, 200);
    }

    public function getDataForDashboard()
    {
        $laporanKerusakan = LaporanKerusakan::where('status', '!=','selesai')
                                                ->where('created_at', '>', Carbon::yesterday())
                                                ->where('created_at', '<', Carbon::Tomorrow())
                                                ->count();
        $laporanSelesai = LaporanKerusakan::where('status', '=','selesai')
                                                ->where('created_at', '>', Carbon::yesterday())
                                                ->where('created_at', '<', Carbon::Tomorrow())
                                                ->count();
        $pengajuanSparepart = PengajuanSparepart::where('created_at', '>', Carbon::yesterday())
                                                ->where('created_at', '<', Carbon::Tomorrow())
                                                ->count();
        $pengajuanJasa = PengajuanJasa::where('created_at', '>', Carbon::yesterday())
                                                ->where('created_at', '<', Carbon::Tomorrow())
                                                ->count();

        $data[0] = [
            'title' => 'LAPORAN KERUSAKAN',
            'amount' => $laporanKerusakan,
            'desc' => 'JUMLAH HARI INI'
        ];
        $data[1] = [
            'title' => 'LAPORAN SELESAI',
            'amount' => $laporanSelesai,
            'desc' => 'JUMLAH HARI INI'
        ];
        $data[2] = [
            'title' => 'PENGAJUAN SPAREPART',
            'amount' => $pengajuanSparepart,
            'desc' => 'JUMLAH HARI INI'
        ];
        $data[3] = [
            'title' => 'PENGAJUAN JASA',
            'amount' => $pengajuanJasa,
            'desc' => 'JUMLAH HARI INI'
        ];

        return response()->json($data, 200);
    }

    public function getReportDaily()
    {
        $laporanDibuat = LaporanKerusakan::reportLaporanDaily();
        $laporanSelesai = LaporanKerusakan::reportLaporanDaily('selesai');
        $laporanDibuatTemp = [];
        $laporanSelesaiTemp = [];
        foreach ($laporanDibuat as $key => $value) {
            $laporanDibuatTemp[(int)$key] = count($value);
        }
        foreach ($laporanSelesai as $key => $value) {
            $laporanSelesaiTemp[(int)$key] = count($value);
        }
        $reportDaily = [];
        for ($i=1; $i <= date('d'); $i++) { 
            
            // if(!empty($laporanDibuatTemp[$i])) {
            //     echo $laporanDibuatTemp[$i];
            // }
            $reportDaily[] = [
                'laporan_dibuat' => empty($laporanDibuatTemp[$i]) ? 0 : $laporanDibuatTemp[$i],
                'laporan_selesai' => empty($laporanSelesaiTemp[$i]) ? 0 : $laporanSelesaiTemp[$i],
                'tgl' => date("$i-m-Y")
            ];
        }
        return response()->json($reportDaily);
    }

    public function getReportUser($userId)
    {
        $laporanDibuat = LaporanKerusakan::reportByUser($userId);
        $laporanDiselesaikan = LaporanKerusakan::reportFinishByUser($userId);
        $laporanDibuatTemp = [];
        $laporanSelesaiTemp = [];

        foreach ($laporanDibuat as $key => $value) {
            $laporanDibuatTemp[(int)$key] = count($value);
        }
        foreach ($laporanDiselesaikan as $key => $value) {
            $laporanSelesaiTemp[(int)$key] = count($value);
        }
        $reportDaily = [];
        for ($i=1; $i <= date('m'); $i++) { 
            
            // if(!empty($laporanDibuatTemp[$i])) {
            //     echo $laporanDibuatTemp[$i];
            // }
            $reportDaily[] = [
                'tgl' => date("$i-Y"),
                'laporan_dibuat' => empty($laporanDibuatTemp[$i]) ? 0 : $laporanDibuatTemp[$i],
                'laporan_selesai' => empty($laporanSelesaiTemp[$i]) ? 0 : $laporanSelesaiTemp[$i]
            ];
        }
        return response()->json($reportDaily);
    }

    public function getByDate($firstDate, $lastDate)
    {
        $laporan = LaporanKerusakan::with(['sub_mesin', 'user', 'penyelesai', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line.departemen', 'penggantian_sparepart.sparepart', 'jasa', 'pengajuan_sparepart', 'pengajuan_sparepart.detail.sparepart'])
                                    ->whereDate('created_at', '>=',  Carbon::parse($firstDate)->format('Y-m-d'))
                                    ->whereDate('created_at', '<=', Carbon::parse($lastDate)->format('Y-m-d'))
                                    ->get();
        $laporanResource = LaporanKerusakanResource::collection($laporan);
        $response = [
            'status' => true,
            'message' => "berhasil mengambil laporan",
            'data' => $laporanResource
        ];
        return $response;
    }

    public function getBySubMesin($subMesinId)
    {
        $laporan = LaporanKerusakanResource::collection(LaporanKerusakan::with(['sub_mesin', 'user', 'penyelesai', 'sub_mesin.mesin', 'sub_mesin.mesin.group', 'sub_mesin.mesin.line', 'sub_mesin.mesin.line.departemen'])->where('sub_mesin_id', $subMesinId)->orderBy('created_at', 'desc')->get());
        $response = [
            'status' => true,
            'message' => "berhasil mengambil laporan",
            'data' => $laporan
        ];
        return $response;
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(40) . '.' . $photo->extension();
        $destinationPath = storage_path() . DIRECTORY_SEPARATOR . 'app/public/img/laporan-kerusakan';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }
}
