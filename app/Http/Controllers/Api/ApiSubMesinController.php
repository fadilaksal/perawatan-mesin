<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\SubMesin;
use App\Http\Resources\SubMesinResource;

class ApiSubMesinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil laporan",
            'data' => SubMesinResource::collection(SubMesin::with(['mesin', 'mesin.group', 'mesin.line', 'mesin.line.departemen'])->get())
        ];
        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil sub mesin",
            'data' => new SubMesinResource(SubMesin::with(['mesin', 'mesin.group', 'mesin.line', 'mesin.line.departemen'])->findOrFail($id))
        ];
        return $response;
    }
    
    public function getByMesinId($mesinId)
    {
        $response = [
            'status' => true,
            'message' => "berhasil mengambil sub mesin",
            'data' => SubMesinResource::collection(SubMesin::where('mesin_id', $mesinId)->get())
        ];
        return $response;
    }
}
