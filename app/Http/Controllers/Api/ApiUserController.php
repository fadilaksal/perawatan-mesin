<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Http\Resources\UserResource;

class ApiUserController extends Controller
{
    private $responseStatus = 200;

    public function index()
    {
        $user = User::with('line')->get();
        $response = [
            'message' => "User Ditemukan",
            'data' => UserResource::collection($user),
            'status' => true,
        ];

        return response()->json($response, $this->responseStatus);
    }

    public function store(Request $request)
    {
        
    }

    public function show($id)
    {
        $user = User::find($id);
        if($user){
            $response = [
                'message' => "User Ditemukan",
                'data' => new UserResource($user),
                'status' => true,
            ];
        } else {
            $response = [
                'message' => "User Tidak Ditemukan",
                'data' => null,
                'status' => false,
            ];

            $this->responseStatus = 400;
        }

        return response()->json($response, $this->responseStatus);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
