<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mesin;
use App\GroupMesin;
use App\Line;
use App\Imports\MesinImport;

class MesinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mesin = Mesin::paginate(15);
        $groupMesin = GroupMesin::all();
        $line = Line::all();
        return view('layouts.pages.mesin.index', compact('mesin', 'groupMesin', 'line'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'group' => 'required|int|exists:group_mesin,id',
            'line' => 'required|int|exists:lines,id',
            'name' => 'required|string|max:191'
        ]);
        
        $mesin = Mesin::create([
            'group_id' => $request->group,
            'line_id' => $request->line,
            'name' => $request->name
        ]);

        flash("Berhasil menambahkan data Mesin : $request->name")->success();

        return redirect()->route('mesin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'group' => 'required|int|exists:group_mesin,id',
            'line' => 'required|int|exists:lines,id',
            'name' => 'required|string|max:191'
        ]);

        $mesin = Mesin::findOrFail($id);
        
        $mesin->group_id = $request->group != null ? $request->group : $mesin->group_id;
        $mesin->line_id = $request->line != null ? $request->line : $mesin->line_id;
        $mesin->name = $request->name != null ? $request->name : $mesin->name;
        
        $mesin->update();

        flash("Berhasil mengubah data Mesin : $request->name")->success();

        return redirect()->route('mesin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mesin = Mesin::findOrFail($id);

        flash("Berhasil menghapus Mesin : $mesin->name")->success();

        $mesin->delete();

        return redirect()->route('mesin');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);

        if($request->hasFile('file')) {
            $file = $request->file('file');
 
            $nama_file = rand().$file->getClientOriginalName();
    
            $file->move('file_mesin',$nama_file);
    
            $import = new MesinImport();
            $import->import(public_path("/file_mesin/".$nama_file));
    
            flash("Berhasil import data")->success();
    
            return redirect()->back();
        } else {
            flash("File belum dipilih")->error();
        }
    }
}
