<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Line;
use Illuminate\Support\Facades\Hash;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::paginate(15);
        $line = Line::all();
        return view('layouts.pages.user.index', compact('user', 'line'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users',
            'employee_id' => 'required|string|max:191|unique:users,employee_id',
            'password' => 'required|string|min:8|confirmed',
            'role' => 'required|string|max:191',
            'line' => 'required|int|exists:lines,id',
        ]);
        
        $user = User::create([
            'employee_id' => $request->employee_id,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role,
            'line_id' => $request->line,
            'email_verified_at' => date('Y-m-d h:i:s')
        ]);

        flash("Berhasil menambah user $user->name")->success();

        return redirect()->route('user');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'employee_id' => 'required|string|max:191',
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191',
            'role' => 'required|string|max:191',
            'line' => 'required|int|exists:lines,id',
        ]);
        
        $user = User::findOrFail($id);
    
        $user->employee_id = $request->employee_id != null ? $request->employee_id : $user->employee_id;
        $user->name = $request->name != null ? $request->name : $user->name;
        $user->email = $request->email != null ? $request->email : $user->email;
        $user->role = $request->role != null ? $request->role : $user->role;
        $user->line_id = $request->line != null ? $request->line : $user->line_id;

        $user->update();

        flash("Berhasil mengubah user $user->name")->success();

        return redirect()->route('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        flash("Berhasil menghapus user $user->name")->success();

        $user->delete();

        return redirect()->route('user');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);

        if($request->hasFile('file')) {
            $file = $request->file('file');
 
            $nama_file = rand().$file->getClientOriginalName();
    
            $file->move('file_user',$nama_file);
    
            $import = new UsersImport();
            $import->import(public_path("/file_user/".$nama_file));
    
            flash("Berhasil import data")->success();
    
            return redirect()->back();
        } else {
            flash("File belum dipilih")->error();
        }
    }
}
