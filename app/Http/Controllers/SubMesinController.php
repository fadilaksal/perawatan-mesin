<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubMesin;
use App\Mesin;
use App\Imports\SubMesinImport;

class SubMesinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_mesin = SubMesin::paginate(15);
        $mesin = Mesin::all();
        return view('layouts.pages.sub_mesin.index', compact('sub_mesin', 'mesin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'mesin' => 'required|int|exists:mesin,id',
            'name' => 'required|string|max:191'
        ]);
        
        $sub_mesin = SubMesin::create([
            'mesin_id' => $request->mesin,
            'name' => $request->name
        ]);

        flash("Berhasil menambahkan data sub_mesin : $request->name")->success();

        return redirect()->route('sub_mesin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'mesin' => 'required|int|exists:mesin,id',
            'name' => 'required|string|max:191'
        ]);

        $sub_mesin = SubMesin::findOrFail($id);
        
        $sub_mesin->mesin_id = $request->mesin != null ? $request->mesin : $sub_mesin->mesin_id;
        $sub_mesin->name = $request->name != null ? $request->name : $sub_mesin->name;
        
        $sub_mesin->update();

        flash("Berhasil mengubah data sub_mesin : $request->name")->success();

        return redirect()->route('sub_mesin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub_mesin = SubMesin::findOrFail($id);

        flash("Berhasil menghapus sub_mesin : $sub_mesin->name")->success();

        $sub_mesin->delete();

        return redirect()->route('sub_mesin');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);

        if($request->hasFile('file')) {
            $file = $request->file('file');
 
            $nama_file = rand().$file->getClientOriginalName();
    
            $file->move('file_sub_mesin',$nama_file);
    
            $import = new SubMesinImport();
            $import->import(public_path("/file_sub_mesin/".$nama_file));
    
            flash("Berhasil import data")->success();
    
            return redirect()->back();
        } else {
            flash("File belum dipilih")->error();
        }
    }
}
