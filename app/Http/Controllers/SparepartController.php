<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sparepart;
use App\Imports\SparepartImport;

class SparepartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sparepart = Sparepart::paginate(15);
        return view('layouts.pages.sparepart.index', compact('sparepart'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'material' => 'required|string|max:191',
            'description' => 'required|string|max:191',
            'quantity' => 'required|int|min:0',
            'base_unit' => 'required|string|max:191'
        ]);
        
        $sparepart = Sparepart::create($request->all());

        flash("Berhasil menambah Sparepart $sparepart->description")->success();

        return redirect()->route('sparepart');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'material' => 'required|string|max:191',
            'description' => 'required|string|max:191',
            'quantity' => 'required|int|min:0',
            'base_unit' => 'required|string|max:191'
        ]);
        
        $sparepart = Sparepart::findOrFail($id);

        $sparepart->update($request->all());

        flash("Berhasil mengubah Sparepart $sparepart->description")->success();

        return redirect()->route('sparepart');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sparepart = Sparepart::findOrFail($id);

        flash("Berhasil menghapus Sparepart $sparepart->description")->success();

        $sparepart->delete();

        return redirect()->route('sparepart');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);

        if($request->hasFile('file')) {
            $file = $request->file('file');
 
            $nama_file = rand().$file->getClientOriginalName();
    
            $file->move('file_sparepart',$nama_file);
    
            $import = new SparepartImport();
            $import->import(public_path("/file_sparepart/".$nama_file));
    
            flash("Berhasil import data")->success();
    
            return redirect()->back();
        } else {
            flash("File belum dipilih")->error();
        }
    }
}
