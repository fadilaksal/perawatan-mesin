@extends('layouts.master')

@section('page', 'Detail Laporan')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h5 class="m-0">{{ $laporan->keterangan }} @if($laporan->tipe == "urgent")<span class="badge bg-danger">{{ strtoupper($laporan->tipe) }}</span>@endif</h5>
            </div>
            <div class="card-body p-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="width: 250px">Mesin</td>
                            <td style="width: 5px">:</td>
                            <td>{{ $laporan->sub_mesin->name }}</td>
                        </tr>
                        <tr>
                            <td style="width: 250px">Departemen</td>
                            <td style="width: 5px">:</td>
                            <td>{{ $laporan->sub_mesin->mesin->line->departemen->name }} - {{ $laporan->sub_mesin->mesin->line->name }}</td>
                        </tr>
                        <tr>
                            <td style="width: 250px">Pelapor</td>
                            <td style="width: 5px">:</td>
                            <td><a href="{{ route('user.show', $laporan->user->id) }}">{{ $laporan->user->employee_id }} - {{ ucfirst($laporan->user->name) }}</a></td>
                        </tr>
                        <tr>
                            <td style="width: 250px">Status</td>
                            <td style="width: 5px">:</td>
                            <td>
                                @if($laporan->status == "baru")
                                    <span class="badge bg-info">{{ ucfirst($laporan->status) }}</span>
                                @elseif($laporan->status == "proses")
                                    <span class="badge bg-warning">{{ ucfirst($laporan->status) }}</span>
                                @elseif($laporan->status == "selesai")
                                    <span class="badge bg-success">{{ ucfirst($laporan->status) }}</span>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
              </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h5 class="m-0">Sparepart yang diganti</h5>
                
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                </div>
            </div>
            <div class="card-body p-0">
                <table class="table">
                    @if($laporan->butuhSparepart)
                    <thead>
                        <tr>
                            <th>No. Material</th>
                            <th>Deskripsi</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($laporan->penggantian_sparepart as $item)
                            <tr>
                                <td>{{ $item->sparepart->material }}</td>
                                <td>{{ $item->sparepart->description }}</td>
                                <td>{{ $item->jumlah }} {{ $item->sparepart->base_unit }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    @else
                        <tr>
                            <td colspan="3">Tidak ada sparepart yang diganti</td>
                        </tr>
                    @endif
                </table>
              </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h5 class="m-0">Pengajuan Sparepart</h5>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                </div>
            </div>
            <div class="card-body p-0">
                <table class="table">
                    <tbody>
                        @if($laporan->pengajuan_sparepart)
                            <tr>
                                <td style="width: 250px">PO</td>
                                <td style="width: 5px">:</td>
                                <td>{{ $laporan->pengajuan_sparepart->PO }}</td>
                            </tr>
                            <tr>
                                <td style="width: 250px">Status Pengajuan</td>
                                <td style="width: 5px">:</td>
                                <td>
                                    @php
                                        $leader = $laporan->pengajuan_sparepart->leader == 0 ? 0 : 100; 
                                        $supervisor = $laporan->pengajuan_sparepart->supervisor == 0 ? 0 : 100; 
                                        $manajer = $laporan->pengajuan_sparepart->manajer == 0 ? 0 : 100; 
                                    @endphp
                                    <div class="progress m-1" style="width: 60px; float:left;">
                                        <div class="progress-bar progress-bar-striped bg-success text-black" role="progressbar" style="width: {{$leader}}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">Leader</div>
                                    </div>
                                    <div class="progress m-1" style="width: 60px; float:left;">
                                        <div class="progress-bar progress-bar-striped bg-success text-black" role="progressbar" style="width: {{$supervisor}}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">Supervisor</div>
                                    </div>
                                    <div class="progress m-1" style="width: 60px; float:left;">
                                        <div class="progress-bar progress-bar-striped bg-success text-black" role="progressbar" style="width: {{$manajer}}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">Manajer</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 250px">Sparepart yang diajukan </td>
                                <td style="width: 5px">:</td>
                                <td>
                                    <table class="table table-bordered table-striped">
                                        <thead class="bg-primary">
                                            <tr>
                                                <th>No. Material</th>
                                                <th>Deskripsi</th>
                                                <th>Jumlah</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($laporan->pengajuan_sparepart->detail as $item)
                                            <tr>
                                                <td>{{$item->sparepart->material}}</td>
                                                <td>{{$item->sparepart->description}}</td>
                                                <td>{{$item->jumlah}} {{$item->sparepart->base_unit}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td colspan="3">Tidak ada pengajuan sparepart</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
              </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h5 class="m-0">Pengajuan Jasa</h5>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                </div>
            </div>
            <div class="card-body p-0">
                <table class="table">
                    <tbody>
                        @if($laporan->butuhJasa)
                            <tr>
                                <td style="width: 250px">PO</td>
                                <td style="width: 5px">:</td>
                                <td>{{ $laporan->jasa->PO }}</td>
                            </tr>
                            <tr>
                                <td style="width: 250px">Vendor</td>
                                <td style="width: 5px">:</td>
                                <td>{{ $laporan->jasa->vendor }}</td>
                            </tr>
                            <tr>
                                <td style="width: 250px">Status Pengajuan</td>
                                <td style="width: 5px">:</td>
                                <td>
                                    @php
                                        $leader = $laporan->jasa->leader == 0 ? 0 : 100; 
                                        $supervisor = $laporan->jasa->supervisor == 0 ? 0 : 100; 
                                        $manajer = $laporan->jasa->manajer == 0 ? 0 : 100; 
                                    @endphp
                                    <div class="progress m-1" style="width: 60px; float:left;">
                                        <div class="progress-bar progress-bar-striped bg-success text-black" role="progressbar" style="width: {{$leader}}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">Leader</div>
                                    </div>
                                    <div class="progress m-1" style="width: 60px; float:left;">
                                        <div class="progress-bar progress-bar-striped bg-success text-black" role="progressbar" style="width: {{$supervisor}}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">Supervisor</div>
                                    </div>
                                    <div class="progress m-1" style="width: 60px; float:left;">
                                        <div class="progress-bar progress-bar-striped bg-success text-black" role="progressbar" style="width: {{$manajer}}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">Manajer</div>
                                    </div>
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td colspan="3">Tidak ada pengajuan sparepart</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
              </div>
        </div>
    </div>
</div>
@endsection