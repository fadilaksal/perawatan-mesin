@extends('layouts.master')

@section('page', 'Laporan')

@section('content')
<div class="row">
    <div class="col-12">
    <div class="card">
        <div class="card-header">
        <div class="card-title">
            <a class="btn btn-success" href="{{ route('laporan.export') }}">
                <i class="fas fa-file-excel"></i> Export
            </a>
        </div>

        <div class="card-tools p-2">
            <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
            </div>
        </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Tanggal Dibuat</th>
                    <th>Keterangan</th>
                    <th>Sub Mesin</th>
                    <th>Pelapor</th>
                    <th>Tipe</th>
                    <th>Kebutuhan</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($laporan as $item)
                <tr data-laporan="{{ $item }}">
                    <td>{{ $item->id }}</td>
                    <td>{{ date('d-F-Y H:i', strtotime($item->created_at)) }}</td>
                    <td>{{ $item->keterangan }}</td>
                    <td>{{ $item->sub_mesin->name }}</td>
                    <td>{{ $item->user->name }}</td>
                    <td>
                        @if($item->tipe == "urgent")
                            <span class="badge bg-danger">{{ $item->tipe }}</span>
                        @else
                            <span class="badge bg-primary">{{ $item->tipe }}</span>
                        @endif
                    </td>
                    <td>
                        @if($item->butuhSparepart && $item->butuhJasa)
                            <a href="#" class="badge bg-orange">Sparepart</a> 
                            <a href="#" class="badge bg-warning">Jasa</a>
                        @elseif($item->butuhSparepart)
                            <a href="#" class="badge bg-orange">Sparepart</a>
                        @elseif($item->butuhJasa)
                            <a href="#" class="badge bg-warning">Jasa</a>
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        @if($item->status == "baru")
                            <span class="badge bg-info">{{ ucfirst($item->status) }}</span>
                        @elseif($item->status == "proses")
                            <span class="badge bg-warning">{{ ucfirst($item->status) }}</span>
                        @elseif($item->status == "selesai")
                            <span class="badge bg-success">{{ ucfirst($item->status) }}</span>
                        @endif
                    </td>
                    <td>
                        <!-- <button type="button" class="btn btn-success btn-edit" data-toggle="modal" data-target="#editModal">Edit</button> -->
                        <a href="{{ route('laporan.show', $item->id) }}" class="btn btn-primary btn-sm">Detail</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            {{ $laporan->links() }}
        </div>
        <!-- /.card-footer -->
    </div>
    <!-- /.card -->
    </div>
</div><!-- /.row -->

<!-- modal edit -->
<!-- <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit laporan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" id="form-edit" method="POST" action="#">
                <div class="modal-body">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <input type="text" class="form-control" name="keterangan" id="edit-keterangan" placeholder="Keterangan" value="{{ old('keterangan') }}" required>
                        @if ($errors->first('keterangan'))
                        <small class="text-danger">{{ $errors->first('keterangan') }}</small>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div> -->
@endsection

@section('custom_js')
<script>
$('.btn-edit').click(function(){
    // let laporan = $(this).closest('tr').data('laporan')
    // $('#edit-name').val(laporan.name)
    // $('#form-edit').attr('action', "{{ route('laporan') }}/" + laporan.id)
})
</script>
@endsection