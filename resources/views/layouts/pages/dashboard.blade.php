@extends('layouts.master')

@section('content')
<!-- Info boxes -->
<div class="row">
    <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box">
        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

        <div class="info-box-content">
        <span class="info-box-text">Karyawan</span>
        <span class="info-box-number">
            {{ App\User::count() }}
        </span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box mb-3">
        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-archive"></i></span>

        <div class="info-box-content">
        <span class="info-box-text">Laporan Kerusakan</span>
        <span class="info-box-number">{{ \App\LaporanKerusakan::count() }}</span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix hidden-md-up"></div>

    <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box mb-3">
        <span class="info-box-icon bg-success elevation-1"><i class="fa fa-shopping-cart"></i></span>

        <div class="info-box-content">
        <span class="info-box-text">Pengajuan Sparepart</span>
        <span class="info-box-number">{{ \App\PengajuanSparepart::count() }}</span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box mb-3">
        <span class="info-box-icon elevation-1" style="background:purple;color:white;"><i class="fas fa-hammer"></i></span>

        <div class="info-box-content">
        <span class="info-box-text">Pengajuan Jasa</span>
        <span class="info-box-number">{{ \App\PengajuanJasa::count() }}</span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
    <div class="card">
        <div class="card-header">
        <h5 class="card-title">Grafik Kerusakan</h5>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-widget="remove">
            <i class="fa fa-times"></i>
            </button>
        </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        <div class="row">
            <div class="col-md-8">
            <p class="text-center">
                <strong>Grafik 1 Agustus, 2019 - 8 Agustus, 2019</strong>
            </p>

            <div class="chart">
                <!-- Sales Chart Canvas -->
                <canvas id="laporanChart" height="150" style="height: 250px;"></canvas>
            </div>
            <!-- /.chart-responsive -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
                <p class="text-center">
                    <strong>Laporan Kerusakan</strong>
                </p>

                <div class="progress-group">
                    Laporan Baru
                    <span class="float-right"><b>{{ \App\LaporanKerusakan::where('status', 'baru')->count() }}</b>/{{ \App\LaporanKerusakan::count() }}</span>
                    <div class="progress progress-sm">
                    <div class="progress-bar bg-primary" style="width: @php echo \App\LaporanKerusakan::where('status', 'baru')->count()/\App\LaporanKerusakan::count()*100 @endphp%"></div>
                    </div>
                </div>
                <!-- /.progress-group -->

                <div class="progress-group">
                    Laporan Progress
                    <span class="float-right"><b>{{\App\LaporanKerusakan::where('status', 'proses')->count()}}</b>/{{\App\LaporanKerusakan::count()}}</span>
                    <div class="progress progress-sm">
                    <div class="progress-bar bg-danger" style="width: @php echo \App\LaporanKerusakan::where('status', 'proses')->count()/\App\LaporanKerusakan::count()*100 @endphp%"></div>
                    </div>
                </div>

                <!-- /.progress-group -->
                <div class="progress-group">
                    <span class="progress-text">Laporan Selesai</span>
                    <span class="float-right"><b>{{\App\LaporanKerusakan::where('status', 'selesai')->count()}}</b>/{{\App\LaporanKerusakan::count()}}</span>
                    <div class="progress progress-sm">
                    <div class="progress-bar bg-success" style="width: @php echo \App\LaporanKerusakan::where('status', 'selesai')->count()/\App\LaporanKerusakan::count()*100 @endphp%"></div>
                    </div>
                </div>
                <!-- /.progress-group -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.card-footer -->
    </div>
    <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<div class="row"></div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Laporan Kerusakan</h5>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-widget="remove">
                    <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Aksi</th>
                                    <th>Keterangan</th>
                                    <th>Tipe</th>
                                    <th>Status</th>
                                    <th>Pelapor</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($laporan as $item)
                                <tr>
                                    <td>{{ $item->created_at }}</td>
                                    <td>{{ $item->aksi }}</td>
                                    <td>{{ $item->keterangan }}</td>
                                    <td>
                                        @php
                                        if($item->tipe == "high") $class = "danger";
                                        else $class = "info";
                                        @endphp
                                        <span class="badge badge-{{$class}}" style="color:white;">{{$item->tipe}}</span>
                                    </td>
                                    <td>
                                        @php
                                        if($item->status == "baru") $class = "info";
                                        else if($item->status =="proses") $class = "warning";
                                        else $class = "success";
                                        @endphp
                                        <span class="badge badge-{{$class}}" style="color:white;">{{$item->status}}</span>
                                    </td>
                                    <td>{{ $item->user->name }}</td>
                                    <td>
                                        <a class="btn btn-info btn-sm" href="{{route('laporan.show', $item->id)}}" style="color:white;">Detail</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
@endsection

@section('custom_js')
<script>

// Get context with jQuery - using jQuery's .get() method.
var pendaftaranChartCanvas = $('#laporanChart').get(0).getContext('2d')

var pendaftaranChartOptions = {
    //Boolean - If we should show the scale at all
    showScale               : true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines      : false,
    //String - Colour of the grid lines
    scaleGridLineColor      : 'rgba(0,0,0,.05)',
    //Number - Width of the grid lines
    scaleGridLineWidth      : 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines  : true,
    //Boolean - Whether the line is curved between points
    bezierCurve             : true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension      : 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot                : false,
    //Number - Radius of each point dot in pixels
    pointDotRadius          : 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth     : 1,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius : 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke           : true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth      : 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill             : true,
    //String - A legend template
    legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%=datasets[i].label%></li><%}%></ul>',
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio     : false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive              : true
}
var pendaftaranChart;

$.get("{{ url('api/laporan/report/daily') }}", function(result){
    // console.log(data)

    let label = [], dataLaporanDibuat = [], dataLaporanSelesai = [], datadosen = [], datastaff = []

    for (const key in result) {
        console.log(result[key])
        label.push(result[key].tgl)
        dataLaporanDibuat.push(result[key].laporan_dibuat)
        dataLaporanSelesai.push(result[key].laporan_selesai)
    }


    var pendaftaranChartData = {
        labels  : label,
        datasets: [
            {
                label               : 'Jumlah Laporan Dibuat',
                data                : dataLaporanDibuat,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                ],
                borderWidth: 1
            },
            {
                label               : 'Jumlah Laporan Selesai',
                data                : dataLaporanSelesai,
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                ],
                borderWidth: 1
            }
        ]
    }

    pendaftaranChart = new Chart(pendaftaranChartCanvas , {
        type: "line",
        data: pendaftaranChartData, 
        options: pendaftaranChartOptions
    });
})

</script>
@endsection