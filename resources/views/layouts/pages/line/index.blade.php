@extends('layouts.master')

@section('page', 'Line')

@section('content')
<div class="row">
    <div class="col-12">
    <div class="card">
        <div class="card-header">
        <div class="card-title">
            <button class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                <i class="fas fa-plus"></i> Tambah Data
            </button>
        </div>

        <div class="card-tools p-2">
            <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
            </div>
        </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama Departemen</th>
                    <th>Nama Line</th>
                    <th>Tanggal Dibuat</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($line as $item) 
                <tr data-line="{{ $item }}">
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->departemen->name }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ date('d F Y - H:i:s', strtotime($item->created_at)) }}</td>
                    <td>
                        <button type="button" class="btn btn-success btn-edit" data-toggle="modal" data-target="#editModal">Edit</button>

                        <form class="form-inline" method="POST" action="{{ route('line.destroy', $item->id) }}" style="margin:none;display:inline;">
                            @csrf
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            {{ $line->links() }}
        </div>
        <!-- /.card-footer -->
    </div>
    <!-- /.card -->
    </div>
</div><!-- /.row -->

<!-- modal create -->
<div class="modal fade" id="createModal" role="dialog" aria-labelledby="createModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">Tambah Line</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" method="POST" action="{{ route('line.store') }}">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="departemen">Nama Departemen</label>
                        <select name="departemen" class="form-control select2" id="departemen" style="width: 100%;" required>
                            <option value="" selected disabled>Pilih Departemen</option>
                            @foreach($departemen as $item)
                            <option value="{{$item->id}}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">Nama Line</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama Line" value="{{ old('name') }}" required>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal edit -->
<div class="modal fade" id="editModal" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Line</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" id="form-edit" method="POST" action="#">
                <div class="modal-body">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="departemen">Nama Departemen</label>
                        <select name="departemen" class="form-control select2" id="edit-departemen" style="width: 100%;" required>
                            <option value="" disabled>Pilih Departemen</option>
                            @foreach($departemen as $item)
                            <option value="{{$item->id}}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">Nama Line</label>
                        <input type="text" class="form-control" name="name" id="edit-name" placeholder="Nama Departemen" value="{{ old('name') }}" required>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
$('.btn-edit').click(function(){
    let line = $(this).closest('tr').data('line')
    $('#edit-departemen').val(line.departemen_id).trigger('change')
    $('#edit-name').val(line.name)
    $('#form-edit').attr('action', "{{ route('line') }}/" + line.id)
})
$('.select2').select2()
</script>
@endsection