@extends('layouts.master')

@section('page', 'Detail pengajuan')

@section('content')
<style>
    .badge-default{
        background:gray;
        color:white;
    }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body p-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="width: 250px">PO</td>
                            <td style="width: 5px">:</td>
                            <td>{{ $pengajuan->PO }}</td>
                        </tr>
                        <tr>
                            <td style="width: 250px">Status</td>
                            <td style="width: 5px">:</td>
                            <td>{{ $pengajuan->status }}</td>
                        </tr>
                        <tr>
                            <td style="width: 250px">Status Pengajuan</td>
                            <td style="width: 5px">:</td>
                            <td>
                                @php
                                    if($pengajuan->leader > 0) $leader = 'success';
                                    else $leader = 'default';
                                    if($pengajuan->supervisor > 0) $supervisor = 'success';
                                    else $supervisor = 'default';
                                    if($pengajuan->manajer > 0) $manajer = 'success';
                                    else $manajer = 'default';
                                @endphp
                                <label class="badge badge-{{$leader}}">Leader</label>
                                <label class="badge badge-{{$supervisor}}">Supervisor</label>
                                <label class="badge badge-{{$manajer}}">Manajer</label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 250px">Vendor</td>
                            <td style="width: 5px">:</td>
                            <td>{{ $pengajuan->vendor }}</td>
                        </tr>
                    </tbody>
                </table>
              </div>
        </div>
    </div>
</div>
@endsection