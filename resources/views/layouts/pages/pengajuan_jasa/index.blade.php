@extends('layouts.master')

@section('page', 'Pengajuan Jasa')

@section('content')
<div class="row">
    <div class="col-12">
    <div class="card">
        <div class="card-header">
        <div class="card-title">
            <button class="btn btn-success" data-toggle="modal" data-target="#createModal">
                <i class="fas fa-file-excel"></i> Export
            </button>
        </div>

        <div class="card-tools p-2">
            <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
            </div>
        </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Purchase Order</th>
                    <th>ID</th>
                    <th>Laporan</th>
                    <th>Vendor</th>
                    <th>Status</th>
                    <th>Status Pengajuan</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($pengajuanJasa as $item)
                <tr data-pengajuan="{{ $item }}">
                    <td>{{ $item->PO }}</td>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->laporan->keterangan }}</td>
                    <td>{{ $item->vendor }}</td>
                    <td>{{ $item->status }}</td>
                    <td>
                        @php
                            $leader = 'warning'; $supervisor = 'warning'; $manajer = 'warning';
                            if ($item->leader > 0) {
                                $leader = 'success';
                            }

                            if ($item->supervisor > 0) {
                                $supervisor = 'success';
                            }

                            if ($item->manajer > 0) {
                                $manajer = 'success';
                            } 
                        @endphp
                        <span class="badge bg-{{$leader}}">Leader</span> <span class="badge bg-{{$supervisor}}">Supervisor</span> <span class="badge bg-{{$manajer}}">Manajer</span>
                    </td>
                    <td>
                        <button type="button" class="btn btn-success btn-edit btn-sm" data-toggle="modal" data-target="#editModal">Edit</button>
                        <a href="{{ route('pengajuan-jasa.show', $item->id) }}" class="btn btn-primary btn-sm">Detail</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            {{ $pengajuanJasa->links() }}
        </div>
        <!-- /.card-footer -->
    </div>
    <!-- /.card -->
    </div>
</div><!-- /.row -->

<!-- modal edit -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit laporan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" id="form-edit" method="POST" action="#">
                <div class="modal-body">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="po">PO</label>
                        <input type="text" class="form-control" name="po" id="edit-po" placeholder="Purchase Order" value="{{ old('po') }}" required>
                        @if ($errors->first('po'))
                        <small class="text-danger">{{ $errors->first('po') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="vendor">Vendor</label>
                        <input type="text" class="form-control" name="vendor" id="edit-vendor" placeholder="Vendor" value="{{ old('vendor') }}" required>
                        @if ($errors->first('vendor'))
                        <small class="text-danger">{{ $errors->first('vendor') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="edit-status" class="form-control">
                            <option value="baru">Baru</option>
                            <option value="proses">Proses</option>
                            <option value="selesai">Selesai</option>
                        </select>
                        @if ($errors->first('status'))
                        <small class="text-danger">{{ $errors->first('status') }}</small>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
$('.btn-edit').click(function(){
    let pengajuan = $(this).closest('tr').data('pengajuan')
    $('#edit-vendor').val(pengajuan.vendor)
    $('#edit-po').val(pengajuan.PO)
    $('#edit-status').val(pengajuan.status)
    $('#form-edit').attr('action', "{{ route('pengajuan-jasa') }}/" + pengajuan.id)
})
</script>
@endsection