@extends('layouts.master')

@section('page', 'sparepart')

@section('content')
<div class="row">
    <div class="col-12">
    <div class="card">
        <div class="card-header">
        <div class="card-title">
            <button class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                <i class="fas fa-plus"></i> Tambah Data
            </button>
            <button class="btn btn-success" data-toggle="modal" data-target="#importModal">
                <i class="fas fa-file-excel"></i> Import
            </button>
        </div>

        <div class="card-tools p-2">
            <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
            </div>
        </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Material</th>
                    <th>Description</th>
                    <th>Quantity</th>
                    <th>Tanggal Dibuat</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($sparepart as $item)
                <tr data-sparepart="{{ $item }}">
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->material }}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->quantity }} {{ $item->base_unit }}</td>
                    <td>{{ date('d F Y - H:i:s', strtotime($item->created_at)) }}</td>
                    <td>
                        <button type="button" class="btn btn-success btn-edit" data-toggle="modal" data-target="#editModal">Edit</button>

                        <form class="form-inline" method="POST" action="{{ route('sparepart.destroy', $item->id) }}" style="margin:none;display:inline;">
                            @csrf
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            {{ $sparepart->links() }}
        </div>
        <!-- /.card-footer -->
    </div>
    <!-- /.card -->
    </div>
</div><!-- /.row -->

<!-- modal create -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">Tambah Sparepart</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" method="POST" action="{{ route('sparepart.store') }}">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="material">Material</label>
                        <input type="text" class="form-control" name="material" id="material" placeholder="Material" value="{{ old('material') }}" required>
                        @if ($errors->first('material'))
                        <small class="text-danger">{{ $errors->first('material') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="description">Descriotion</label>
                        <input type="text" class="form-control" name="description" id="description" placeholder="Description" value="{{ old('description') }}" required>
                        @if ($errors->first('description'))
                        <small class="text-danger">{{ $errors->first('description') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="quantity">Quantity</label>
                        <input type="number" class="form-control" name="quantity" id="quantity" placeholder="Quantity" value="{{ old('quantity') }}" required min="0">
                        @if ($errors->first('quantity'))
                        <small class="text-danger">{{ $errors->first('quantity') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="base_unit">Base Unit</label>
                        <select class="form-control" name="base_unit" id="base_unit" required>
                            <option value="" selected disabled>Pilih Base Unit</option>
                            <option value="BOT">BOT</option>
                            <option value="BOX">BOX</option>
                            <option value="CAN">CAN</option>
                            <option value="KG">KG</option>
                            <option value="L">L</option>
                            <option value="M">M</option>
                            <option value="M3">M3</option>
                            <option value="mbt">mbt</option>
                            <option value="PAI">PAI</option>
                            <option value="PAK">PAK</option>
                            <option value="PCS">PCS</option>
                            <option value="ROL">ROL</option>
                            <option value="SET">SET</option>
                            <option value="SHT">SHT</option>
                            <option value="TUB">TUB</option>
                            <option value="UN">UN</option>
                        </select>
                        @if ($errors->first('base_unit'))
                        <small class="text-danger">{{ $errors->first('base_unit') }}</small>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal edit -->

<!-- modal create -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Sparepart</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" id="form-edit" method="POST" action="#">
                <div class="modal-body">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="material">Nama sparepart</label>
                        <input type="text" class="form-control" name="material" id="edit-material" placeholder="Material" value="{{ old('material') }}" required>
                        @if ($errors->first('material'))
                        <small class="text-danger">{{ $errors->first('material') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="edit-description">Descriotion</label>
                        <input type="text" class="form-control" name="description" id="edit-description" placeholder="Description" value="{{ old('description') }}" required>
                        @if ($errors->first('description'))
                        <small class="text-danger">{{ $errors->first('description') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="edit-quantity">Quantity</label>
                        <input type="number" class="form-control" name="quantity" id="edit-quantity" placeholder="Quantity" value="{{ old('quantity') }}" required min="0">
                        @if ($errors->first('quantity'))
                        <small class="text-danger">{{ $errors->first('quantity') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="edit-base_unit">Base Unit</label>
                        <select class="form-control" name="base_unit" id="edit-base_unit" required>
                            <option value="" selected disabled>Pilih Base Unit</option>
                            <option value="BOT">BOT</option>
                            <option value="BOX">BOX</option>
                            <option value="CAN">CAN</option>
                            <option value="KG">KG</option>
                            <option value="L">L</option>
                            <option value="M">M</option>
                            <option value="M3">M3</option>
                            <option value="mbt">mbt</option>
                            <option value="PAI">PAI</option>
                            <option value="PAK">PAK</option>
                            <option value="PCS">PCS</option>
                            <option value="ROL">ROL</option>
                            <option value="SET">SET</option>
                            <option value="SHT">SHT</option>
                            <option value="TUB">TUB</option>
                            <option value="UN">UN</option>
                        </select>
                        @if ($errors->first('base_unit'))
                        <small class="text-danger">{{ $errors->first('base_unit') }}</small>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal import -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importModalLabel">Import data sparepart</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" id="form-import" method="POST" action="{{ route('sparepart.import') }}" enctype='multipart/form-data'>
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="file">Pilih File</label>
                        <input type="file" name="file" class="form-control" required="required">
                        @error('file')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
$('.btn-edit').click(function(){
    let sparepart = $(this).closest('tr').data('sparepart')
    $('#edit-material').val(sparepart.material)
    $('#edit-description').val(sparepart.description)
    $('#edit-quantity').val(sparepart.quantity)
    $('#edit-base_unit').val(sparepart.base_unit)
    $('#form-edit').attr('action', "{{ route('sparepart') }}/" + sparepart.id)
})
</script>
@endsection