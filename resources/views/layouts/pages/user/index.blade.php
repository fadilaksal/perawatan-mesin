@extends('layouts.master')

@section('page', 'user')

@section('content')
<div class="row">
    <div class="col-12">
    <div class="card">
        <div class="card-header">
        <div class="card-title">
            <button class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                <i class="fas fa-plus"></i> Tambah Data
            </button>
            <button class="btn btn-success" data-toggle="modal" data-target="#importModal">
                <i class="fas fa-file-excel"></i> Import
            </button>
        </div>

        <div class="card-tools p-2">
            <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
            </div>
        </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>Nama</th>
                    <th>Line</th>
                    <th>Role</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($user as $item)
                <tr data-user="{{ $item }}">
                    <td>{{ $item->employee_id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->line->name }}</td>
                    <td>{{ ucfirst($item->role) }}</td>
                    <td>
                        <button type="button" class="btn btn-success btn-edit" data-toggle="modal" data-target="#editModal">Edit</button>

                        <form class="form-inline" method="POST" action="{{ route('user.destroy', $item->id) }}" style="margin:none;display:inline;">
                            @csrf
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            {{ $user->links() }}
        </div>
        <!-- /.card-footer -->
    </div>
    <!-- /.card -->
    </div>
</div><!-- /.row -->

<!-- modal create -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">Tambah user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" method="POST" action="{{ route('user.store') }}">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="employee_id">Employee ID</label>
                        <input type="text" class="form-control" name="employee_id" id="employee_id" placeholder="ID Karyawan" value="{{ old('employee_id') }}" required>
                        @if ($errors->first('employee_id'))
                        <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">Nama user</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama user" value="{{ old('name') }}" required>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-form-label text-md-right">{{ __('E-Mail') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                        @error('email')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">
                        @error('password')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Password Confirmation">
                    </div>

                    <div class="form-group">
                        <label for="role" class="col-form-label text-md-right">{{ __('Role') }}</label>
                        <select class="form-control" name="role" id="role" required>
                            <option value="" selected disabled>Pilih Role</option>
                            <option value="officer">Officer</option>
                            <option value="teknisi">Teknisi</option>
                            <option value="leader">Leader</option>
                            <option value="supervisor">Supervisor</option>
                            <option value="manajer">Manajer</option>
                        </select>
                        @error('role')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="line" class="col-form-label text-md-right">{{ __('Line') }}</label>
                        <select class="form-control" name="line" id="line" required>
                            <option value="" selected disabled>Pilih Line</option>
                            @foreach($line as $item)
                                <option value="{{$item->id}}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('line')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal edit -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" id="form-edit" method="POST" action="#">
                <div class="modal-body">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="edit-employee_id">Employee ID</label>
                        <input type="text" class="form-control" name="employee_id" id="edit-employee_id" placeholder="ID Karyawan" value="{{ old('employee_id') }}" required>
                        @if ($errors->first('employee_id'))
                        <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">Nama user</label>
                        <input type="text" class="form-control" name="name" id="edit-name" placeholder="Nama user" value="{{ old('name') }}" required>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="edit-email" class="col-form-label text-md-right">{{ __('E-Mail') }}</label>
                        <input id="edit-email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                        @error('email')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="role" class="col-form-label text-md-right">{{ __('Role') }}</label>
                        <select class="form-control" name="role" id="edit-role" required>
                            <option value="" selected disabled>Pilih Role</option>
                            <option value="officer">Officer</option>
                            <option value="teknisi">Teknisi</option>
                            <option value="leader">Leader</option>
                            <option value="supervisor">Supervisor</option>
                            <option value="manajer">Manajer</option>
                        </select>
                        @error('role')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="edit-line" class="col-form-label text-md-right">{{ __('Line') }}</label>
                        <select class="form-control select2" name="line" id="edit-line" required>
                            <option value="" selected disabled>Pilih Line</option>
                            @foreach($line as $item)
                                <option value="{{$item->id}}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('line')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal import -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importModalLabel">Import data user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" id="form-import" method="POST" action="{{ route('user.import') }}" enctype='multipart/form-data'>
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="file">Pilih File</label>
                        <input type="file" name="file" class="form-control" required="required">
                        @error('file')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
$('.btn-edit').click(function(){
    let user = $(this).closest('tr').data('user')
    $('#edit-employee_id').val(user.employee_id)
    $('#edit-name').val(user.name)
    $('#edit-email').val(user.email)
    $('#edit-role').val(user.role)
    $('#edit-line').val(user.line_id)
    $('#form-edit').attr('action', "{{ route('user') }}/" + user.id)
})

// $('.table').DataTable()
</script>
@endsection