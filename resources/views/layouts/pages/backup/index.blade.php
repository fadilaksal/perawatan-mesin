@extends('layouts.master')

@section('page', 'Backup')

@section('content')
<div class="row">
    <div class="col-12">
    <div class="card">
        <div class="card-header">
        <div class="card-title">
            <button class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                <i class="fas fa-plus"></i> Backup
            </button>
        </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Ukuran</th>
                    <th>Tanggal Dibuat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php($index = 1)
                @foreach($backups as $item)
                <tr>
                    <td>{{$index}}</td>
                    <td>{{$item['name']}}</td>
                    <td>{{$item['size']}} kb</td>
                    <td>{{$item['created']}}</td>
                    <td class="text-center">
                        <a href="{{ route('backup.restore', $item['name']) }}" class="btn btn-warning btn-sm"><i  class="fas fa-redo-alt text-white"></i></a>
                        <a href="{{ route('backup.download', $item['name']) }}" class="btn btn-primary btn-sm"><i  class="fas fa-download"></i></a>
                        <a href="{{ route('backup.destroy', $item['name']) }}" class="btn btn-danger btn-sm"><i  class="fas fa-trash"></i></a>
                    </td>
                </tr>
                @php($index++)
                @endforeach
            </tbody>
        </table>
        </div>
        <!-- /.card-footer -->
    </div>
    <!-- /.card -->
    </div>
</div><!-- /.row -->

<!-- modal create -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">Backup Database</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" method="POST" action="{{ route('backup.store') }}">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nama Backup</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama Backup" value="{{ old('name') }}" required>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Backup</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
$('.btn-edit').click(function(){
    let departemen = $(this).closest('tr').data('departemen')
    $('#edit-name').val(departemen.name)
    $('#form-edit').attr('action', "{{ route('departemen') }}/" + departemen.id)
})
</script>
@endsection