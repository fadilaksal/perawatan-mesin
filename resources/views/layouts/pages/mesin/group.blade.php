@extends('layouts.master')

@section('page', 'Group Mesin')

@section('content')
<div class="row">
    <div class="col-12">
    <div class="card">
        <div class="card-header">
        <div class="card-title">
            <button class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                <i class="fas fa-plus"></i> Tambah Data
            </button>
        </div>

        <div class="card-tools p-2">
            <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
            </div>
        </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama Group</th>
                    <th>Tanggal Dibuat</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($groupMesin as $item)
                <tr data-group_mesin="{{ $item }}">
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ date('d F Y - H:i:s', strtotime($item->created_at)) }}</td>
                    <td>
                        <button type="button" class="btn btn-success btn-edit" data-toggle="modal" data-target="#editModal">Edit</button>

                        <form class="form-inline" method="POST" action="{{ route('group-mesin.destroy', $item->id) }}" style="margin:none;display:inline;">
                            @csrf
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            {{ $groupMesin->links() }}
        </div>
        <!-- /.card-footer -->
    </div>
    <!-- /.card -->
    </div>
</div><!-- /.row -->

<!-- modal create -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">Tambah group-mesin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" method="POST" action="{{ route('group-mesin.store') }}">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nama group-mesin</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama group-mesin" value="{{ old('name') }}" required>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal edit -->

<!-- modal create -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Group Mesin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" id="form-edit" method="POST" action="#">
                <div class="modal-body">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="name">Nama group-mesin</label>
                        <input type="text" class="form-control" name="name" id="edit-name" placeholder="Nama Group" value="{{ old('name') }}" required>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
$('.btn-edit').click(function(){
    let group_mesin = $(this).closest('tr').data('group_mesin')
    $('#edit-name').val(group_mesin.name)
    $('#form-edit').attr('action', "{{ route('group-mesin') }}/" + group_mesin.id)
})
</script>
@endsection