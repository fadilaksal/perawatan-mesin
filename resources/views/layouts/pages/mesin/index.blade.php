@extends('layouts.master')

@section('page', 'Mesin')

@section('content')
<div class="row">
    <div class="col-12">
    <div class="card">
        <div class="card-header">
        <div class="card-title">
            <button class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                <i class="fas fa-plus"></i> Tambah Data
            </button>
            <button class="btn btn-success" data-toggle="modal" data-target="#importModal">
                <i class="fas fa-file-excel"></i> Import
            </button>
        </div>

        <div class="card-tools p-2">
            <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
            </div>
        </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama mesin</th>
                    <th>Group</th>
                    <th>Line</th>
                    <th>Tanggal Dibuat</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($mesin as $item) 
                <tr data-mesin="{{ $item }}">
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->group->name }}</td>
                    <td>{{ $item->line->name }}</td>
                    <td>{{ date('d F Y - H:i:s', strtotime($item->created_at)) }}</td>
                    <td>
                        <button type="button" class="btn btn-success btn-edit" data-toggle="modal" data-target="#editModal">Edit</button>

                        <form class="form-inline" method="POST" action="{{ route('mesin.destroy', $item->id) }}" style="margin:none;display:inline;">
                            @csrf
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            {{ $mesin->links() }}
        </div>
        <!-- /.card-footer -->
    </div>
    <!-- /.card -->
    </div>
</div><!-- /.row -->

<!-- modal create -->
<div class="modal fade" id="createModal" role="dialog" aria-labelledby="createModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">Tambah data Mesin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" method="POST" action="{{ route('mesin.store') }}">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="group">Group Mesin</label>
                        <select name="group" class="form-control select2" id="group" style="width: 100%;" required>
                            <option value="" selected disabled>Pilih Group Mesin</option>
                            @foreach($groupMesin as $item)
                            <option value="{{$item->id}}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->first('group'))
                        <small class="text-danger">{{ $errors->first('group') }}</small>
                        @endif
                    </div>
                    
                    <div class="form-group">
                        <label for="line">Line</label>
                        <select name="line" class="form-control select2" id="line" style="width: 100%;" required>
                            <option value="" selected disabled>Pilih Line</option>
                            @foreach($line as $item)
                            <option value="{{$item->id}}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->first('line'))
                        <small class="text-danger">{{ $errors->first('line') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">Nama mesin</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama mesin" value="{{ old('name') }}" required>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal edit -->
<div class="modal fade" id="editModal" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit data Mesin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" id="form-edit" method="POST" action="#">
                <div class="modal-body">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="edit-group">Group</label>
                        <select name="group" class="form-control select2" id="edit-group" style="width: 100%;" required>
                            <option value="" disabled>Pilih Group Mesin</option>
                            @foreach($groupMesin as $item)
                            <option value="{{$item->id}}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="line">Line</label>
                        <select name="line" class="form-control select2" id="edit-line" style="width: 100%;" required>
                            <option value="" selected disabled>Pilih Line</option>
                            @foreach($line as $item)
                            <option value="{{$item->id}}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->first('line'))
                        <small class="text-danger">{{ $errors->first('line') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">Nama mesin</label>
                        <input type="text" class="form-control" name="name" id="edit-name" placeholder="Nama groupMesin" value="{{ old('name') }}" required>
                        @if ($errors->first('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal import -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importModalLabel">Import data mesin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" id="form-import" method="POST" action="{{ route('mesin.import') }}" enctype='multipart/form-data'>
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="file">Pilih File</label>
                        <input type="file" name="file" class="form-control" required="required">
                        @error('file')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
$('.btn-edit').click(function(){
    let mesin = $(this).closest('tr').data('mesin')
    $('#edit-group').val(mesin.group_id).trigger('change')
    $('#edit-line').val(mesin.line_id).trigger('change')
    $('#edit-name').val(mesin.name)
    $('#form-edit').attr('action', "{{ route('mesin') }}/" + mesin.id)
})
$('.select2').select2()
</script>
@endsection