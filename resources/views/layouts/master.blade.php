<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }} | @yield('page', 'Admin')</title>
  
  <link rel="stylesheet" href="/css/app.css">
  <link rel="stylesheet" href="/css/plugins.css">
  <style>
    html,
    body{
      height: 100%;
    }

    .fa-circle{
      font-size: 10pt !important;
    }

    .bg-orange{
      background: orange;
      color: #1F2D3D !important;
    }

    .text-black{
      color: #000 !important;
    }
  </style>
  @yield('custom_css')
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  @include('layouts.partials.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('layouts.partials.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@yield('page', 'Dashboard')</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
      @include('flash::message')

      @yield('content')
      </div><!--/. container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  @include('layouts.partials.footer')
</div>
<!-- ./wrapper -->


<!-- for solve error #app vue -->
<div id='app'></div>

<!-- REQUIRED SCRIPTS -->
<script src="/js/app.js"></script>
<script src="/js/plugins.js"></script>

<!-- Custom Scripts -->
@yield('custom_js')

</body>
</html>
