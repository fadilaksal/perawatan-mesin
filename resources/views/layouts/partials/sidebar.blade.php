<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{ route('dashboard') }}" class="brand-link bg-primary">
    <img src="/img/logo-gf.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
          style="opacity: .8">
    <span class="brand-text font-weight-light">Perawatan Mesin</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="/img/profile.png" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ auth()->user()->name }}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- dashboard menu -->
        <li class="nav-item">
          <a href="{{ route('dashboard') }}" class="nav-link  @if(Route::current()->getName() == 'dashboard') active @endif">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <!-- Master -->
        <li class="nav-item has-treeview @if(Route::current()->getPrefix() == '/departemen' || Route::current()->getPrefix() == '/line' || Route::current()->getPrefix() == '/sparepart') menu-open @endif">
          <a href="#" class="nav-link @if(Route::current()->getPrefix() == '/departemen' || Route::current()->getPrefix() == '/line' || Route::current()->getPrefix() == '/sparepart') active @endif">
            <i class="nav-icon fas fa-cog"></i>
            <p>
              Data Master
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <!-- Departemen -->
            <li class="nav-item">
              <a href="{{ route('departemen') }}" class="nav-link @if(Route::current()->getName() == 'departemen') active @endif">
                <i class="nav-icon fas fa-building"></i>
                <p>
                  Departemen
                </p>
              </a>
            </li>
            <!-- Line -->
            <li class="nav-item">
              <a href="{{ route('line') }}" class="nav-link @if(Route::current()->getName() == 'line') active @endif">
                <i class="nav-icon far fa-building"></i>
                <p>
                  Line
                </p>
              </a>
            </li>
            <!-- Sparepart -->
            <li class="nav-item">
              <a href="{{ route('sparepart') }}" class="nav-link @if(Route::current()->getName() == 'sparepart') active @endif">
                <i class="nav-icon fas fa-archive"></i>
                <p>
                  Sparepart
                </p>
              </a>
            </li>
          </ul>
        </li>
        <!-- Mesin -->
        <li class="nav-item has-treeview @if(Route::current()->getPrefix() == '/mesin') menu-open @endif">
          <a href="#" class="nav-link @if(Route::current()->getPrefix() == '/mesin') active @endif">
            <i class="nav-icon fas fa-industry"></i>
            <p>
              Mesin
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('group-mesin') }}" class="nav-link @if(Route::current()->getName() == 'group-mesin') active @endif">
                <i class="far fa-circle nav-icon"></i>
                <p>Group Mesin</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('mesin') }}" class="nav-link @if(Route::current()->getName() == 'mesin') active @endif">
                <i class="far fa-circle nav-icon"></i>
                <p>List Mesin</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('sub_mesin') }}" class="nav-link @if(Route::current()->getName() == 'sub_mesin') active @endif">
                <i class="far fa-circle nav-icon"></i>
                <p>List Sub Mesin</p>
              </a>
            </li>
          </ul>
        </li>
        <!-- Laporan Kerusakan -->
        <li class="nav-item">
          <a href="{{ route('laporan') }}" class="nav-link @if(Route::current()->getName() == 'laporan') active @endif">
            <i class="nav-icon fas fa-exclamation-triangle"></i>
            <p>
              Laporan Kerusakan
            </p>
          </a>
        </li>
        <!-- Pengajuan -->
        <li class="nav-item has-treeview @if(Route::current()->getPrefix() == '/pengajuan-jasa' || Route::current()->getPrefix() == '/pengajuan-sparepart') menu-open @endif">
          <a href="#" class="nav-link @if(Route::current()->getPrefix() == '/pengajuan-jasa') active @endif">
            <i class="nav-icon fas fa-file-signature"></i>
            <p>
              Pengajuan
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('pengajuan-sparepart') }}" class="nav-link @if(Route::current()->getName() == 'pengajuan-sparepart') active @endif">
                <i class="far fa-circle nav-icon"></i>
                <p>Pengajuan Sparepart</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('pengajuan-jasa') }}" class="nav-link @if(Route::current()->getName() == 'pengajuan-jasa') active @endif">
                <i class="far fa-circle nav-icon"></i>
                <p>Pengajuan Jasa</p>
              </a>
            </li>
          </ul>
        </li>
        <!-- Line -->
        <li class="nav-item">
          <a href="{{ route('user') }}" class="nav-link @if(Route::current()->getName() == 'user') active @endif">
            <i class="nav-icon fas fa-users"></i>
            <p>
              Users
            </p>
          </a>
        </li>          

        <li class="nav-item">
          <a href="{{ route('backup') }}"class="nav-link">
            <i class="nav-icon fas fa-redo"></i>
            <p>Backup</p>
          </a>
        </li>
        <!-- log out menu -->
        <li class="nav-item">
          <a href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();" class="nav-link">
            <i class="nav-icon fas fa-key"></i>
            <p>
              {{ __('Logout') }}
            </p>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>