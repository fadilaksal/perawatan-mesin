<nav class="main-header navbar navbar-expand bg-primary navbar-light border-bottom">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <a href="{{ route('dashboard') }}" class="nav-link">Dashboard</a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <a href="{{ route('sub_mesin') }}" class="nav-link">Sub Mesin</a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <a href="{{ route('laporan') }}" class="nav-link">Laporan Kerusakan</a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <a href="{{ route('user') }}" class="nav-link">Users</a>
    </li>
  </ul>
</nav>