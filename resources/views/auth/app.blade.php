<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title', 'Log In')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/css/app.css">
</head>
<body class="hold-transition login-page">
<div class="login-box" style="margin-top:75px;">
  <div class="login-logo">
    <!-- <a href="#">PerawatanMesin</a> -->
    <img src="/img/logo-gf.png" alt="" width="350" style="padding-left:50px;margin:0;">
  </div>
  <!-- /.login-logo -->
    @yield('content')
</div>
<!-- /.login-box -->

<!-- REQUIRED SCRIPTS -->
<script src="/js/app.js"></script>
</body>
</html>