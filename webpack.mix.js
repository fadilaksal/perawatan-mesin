const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .scripts(
        [
            'node_modules/admin-lte/plugins/select2/select2.full.min.js',
            'node_modules/chart.js/dist/Chart.bundle.js',
            'node_modules/chart.js/dist/Chart.min.js'
        ], 'public/js/plugins.js')
    .styles([
        'node_modules/admin-lte/plugins/select2/select2.css',
        'node_modules/chart.js/dist/Chart.min.css' 
    ], 'public/css/plugins.css')
    .sass('resources/sass/app.scss', 'public/css'); 
