<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/line', 'Api\ApiLineController@index');
Route::get('/line/departemen/{departemenId}', 'Api\ApiLineController@getByDepartemenId');
Route::get('/line/{id}', 'Api\ApiLineController@show');

Route::get('/departemen', 'Api\ApiDepartemenController@index');
Route::get('/departemen/{id}', 'Api\ApiDepartemenController@show');

Route::get('/mesin', 'Api\ApiMesinController@index');
Route::get('/mesin/{id}', 'Api\ApiMesinController@show');
Route::get('/mesin/line/{lineId}', 'Api\ApiMesinController@getByLineId');

Route::get('/sub-mesin', 'Api\ApiSubMesinController@index');
Route::get('/sub-mesin/{id}', 'Api\ApiSubMesinController@show');
Route::get('/sub-mesin/mesin/{mesinId}', 'Api\ApiSubMesinController@getByMesinId');

Route::get('/sparepart', 'Api\ApiSparepartController@index');
Route::get('/sparepart/{id}', 'Api\ApiSparepartController@show');

Route::get('/laporan', 'Api\ApiLaporanKerusakanController@index');
Route::get('/laporan/getLast', 'Api\ApiLaporanKerusakanController@getLastLaporan');
Route::get('/laporan/selesai', 'Api\ApiLaporanKerusakanController@getLaporanSelesai');
Route::get('/laporan/report/daily', 'Api\ApiLaporanKerusakanController@getReportDaily');
Route::get('/laporan/report/{userId}/user', 'Api\ApiLaporanKerusakanController@getReportUser');
Route::get('/laporan/filter/date/{firsDate}/{lastDate}', 'Api\ApiLaporanKerusakanController@getByDate');
Route::get('/laporan/selesai/filter/date/{firsDate}/{lastDate}', 'Api\ApiLaporanKerusakanController@getLaporanSelesaiByDate');
Route::get('/laporan/{id}/sub-mesin', 'Api\ApiLaporanKerusakanController@getBySubMesin');
Route::get('/laporan/{id}', 'Api\ApiLaporanKerusakanController@show');

Route::post('/laporan', 'Api\ApiLaporanKerusakanController@store');
Route::post('/laporan/sparepart', 'Api\ApiLaporanKerusakanController@storeWithSparepart');

Route::post('/laporan/proses', 'Api\ApiLaporanKerusakanController@prosesLaporan');
Route::post('/laporan/selesai', 'Api\ApiLaporanKerusakanController@finishLaporan');


Route::get('/pengajuan-sparepart', 'Api\ApiPengajuanSparepartController@index');
Route::get('/pengajuan-sparepart/{id}', 'Api\ApiPengajuanSparepartController@show');
Route::put('/pengajuan-sparepart/{id}/confirm', 'Api\ApiPengajuanSparepartController@confirm');
Route::get('/pengajuan-sparepart/filter/date/{firsDate}/{lastDate}', 'Api\ApiPengajuanSparepartController@getPengajuanByDate');

Route::post('/auth/login', 'Api\ApiAuthorizationController@login');
Route::get('/auth/{id}/logout', 'Api\ApiAuthorizationController@logout');

Route::get('/user', 'Api\ApiUserController@index');
Route::get('/user/{id}/', 'Api\ApiUserController@show');

Route::get('/dashboard', 'Api\ApiLaporanKerusakanController@getDataForDashboard');