<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::group(['middleware' => ['auth']], function(){
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::prefix('departemen')->group(function () {
        Route::get('/', 'DepartemenController@index')->name('departemen');
        Route::post('/', 'DepartemenController@store')->name('departemen.store');
        Route::put('/{id}', 'DepartemenController@update')->name('departemen.update');
        Route::delete('/{id}', 'DepartemenController@destroy')->name('departemen.destroy');
    });

    Route::prefix('line')->group(function () {
        Route::get('/', 'LineController@index')->name('line');
        Route::post('/', 'LineController@store')->name('line.store');
        Route::put('/{id}', 'LineController@update')->name('line.update');
        Route::delete('/{id}', 'LineController@destroy')->name('line.destroy');
    });

    Route::prefix('sparepart')->group(function () {
        // sparepart
        Route::get('/', 'SparepartController@index')->name('sparepart');
        Route::post('/', 'SparepartController@store')->name('sparepart.store');
        Route::post('/import', 'SparepartController@import')->name('sparepart.import');
        Route::put('/{id}', 'SparepartController@update')->name('sparepart.update');
        Route::delete('/{id}', 'SparepartController@destroy')->name('sparepart.destroy');
    });

    Route::prefix('mesin')->group(function () {
        // group mesin
        Route::get('/group', 'GroupMesinController@index')->name('group-mesin');
        Route::post('/group', 'GroupMesinController@store')->name('group-mesin.store');
        Route::put('/group/{id}', 'GroupMesinController@update')->name('group-mesin.update');
        Route::delete('/group/{id}', 'GroupMesinController@destroy')->name('group-mesin.destroy');
        // sub mesin
        Route::get('sub_mesin/', 'SubMesinController@index')->name('sub_mesin');
        Route::post('sub_mesin/import', 'SubMesinController@import')->name('sub_mesin.import');
        Route::get('/{id}/sub_mesin', 'SubMesinController@index')->name('sub_mesin.mesin');
        Route::post('/sub_mesin', 'SubMesinController@store')->name('sub_mesin.store');
        Route::put('/sub_mesin/{id}', 'SubMesinController@update')->name('sub_mesin.update');
        Route::delete('/sub_mesin/{id}', 'SubMesinController@destroy')->name('sub_mesin.destroy');
        // mesin
        Route::post('/import', 'MesinController@import')->name('mesin.import');
        Route::get('/', 'MesinController@index')->name('mesin');
        Route::post('/', 'MesinController@store')->name('mesin.store');
        Route::put('/{id}', 'MesinController@update')->name('mesin.update');
        Route::delete('/{id}', 'MesinController@destroy')->name('mesin.destroy');
    });
    
    Route::prefix('laporan')->group(function () {
        // laporan kerusakan
        Route::get('/', 'LaporanKerusakanController@index')->name('laporan');
        Route::get('/export', 'LaporanKerusakanController@export')->name('laporan.export');
        Route::get('/show/{id}', 'LaporanKerusakanController@show')->name('laporan.show');
        Route::post('/', 'LaporanKerusakanController@store')->name('laporan.store');
        Route::put('/{id}', 'LaporanKerusakanController@update')->name('laporan.update');
        Route::delete('/{id}', 'LaporanKerusakanController@destroy')->name('laporan.destroy');
    });
    
    Route::prefix('user')->group(function () {
        // user
        Route::get('/', 'UserController@index')->name('user');
        Route::get('/show/{id}', 'UserController@show')->name('user.show');
        Route::post('/', 'UserController@store')->name('user.store');
        Route::post('/import', 'UserController@import')->name('user.import');
        Route::put('/{id}', 'UserController@update')->name('user.update');
        Route::delete('/{id}', 'UserController@destroy')->name('user.destroy');
    });

    Route::prefix('pengajuan-jasa')->group(function () {
        // laporan kerusakan
        Route::get('/', 'PengajuanJasaController@index')->name('pengajuan-jasa');
        Route::get('/show/{id}', 'PengajuanJasaController@show')->name('pengajuan-jasa.show');
        Route::put('/{id}', 'PengajuanJasaController@update')->name('pengajuan-jasa.update');
    });

    Route::prefix('pengajuan-sparepart')->group(function () {
        // laporan kerusakan
        Route::get('/', 'PengajuanSparepartController@index')->name('pengajuan-sparepart');
        Route::get('/show/{id}', 'PengajuanSparepartController@show')->name('pengajuan-sparepart.show');
        Route::put('/{id}', 'PengajuanSparepartController@update')->name('pengajuan-sparepart.update');
    });

    Route::prefix('backup')->group(function () {
        Route::get('/', 'BackupController@index')->name('backup');
        Route::get('/download/{fileName}', 'BackupController@download')->name('backup.download');
        Route::post('/', 'BackupController@store')->name('backup.store');
        Route::get('/restore/{fileName}', 'BackupController@restore')->name('backup.restore');
        Route::get('/destroy/{fileName}', 'BackupController@destroy')->name('backup.destroy');
    });

    // Route::prefix('departemen')->group(function () {
    //     Route::get('users', function () {
    //         // Matches The "/admin/users" URL
    //     });
    // });
});